import { NgModule } from '@angular/core';
import { ForbiddenValidatorDirective } from './forbiddenName/forbidden.directive';
import { MaskingDirective } from './masking/masking';
import { IntegerDirective } from './integer/integer';
import { RepassDirective } from './repass/repass';
import { UsernameCheckDirective } from './username-check/username-check';
import { EmailCheckDirective } from './email-check/email-check';
import { AutosizeDirective } from './autosize/autosize';
import { CheckAlamatwithGoogleApiDirective } from './check-alamatwith-google-api/check-alamatwith-google-api';
import { PhonenumberCheckDirective } from './phonenumber-check/phonenumber-check';

@NgModule({
	declarations: [
		ForbiddenValidatorDirective,
    MaskingDirective,
    IntegerDirective,
    RepassDirective,
    UsernameCheckDirective,
    EmailCheckDirective,
    AutosizeDirective,
    CheckAlamatwithGoogleApiDirective,
    PhonenumberCheckDirective
	],
	imports: [],
	exports: [
		ForbiddenValidatorDirective,
    MaskingDirective,
    IntegerDirective,
    RepassDirective,
    UsernameCheckDirective,
    EmailCheckDirective,
    AutosizeDirective,
    CheckAlamatwithGoogleApiDirective,
    PhonenumberCheckDirective
	]
})
export class DirectivesModule { }
