import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn } from '@angular/forms';
import { NgModel } from '@angular/forms';

/**
 * Generated class for the RepassDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
export function repassValidator(repass: any): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } => {
    const pass = repass!=control.value;
    return pass ? { 'pass': { value: control.value } } : null;
  };
}

@Directive({
  selector: '[repass]', // Attribute selector
  providers: [{ provide: NG_VALIDATORS, useExisting: RepassDirective, multi: true }]
})
export class RepassDirective implements Validator {
  @Input('pass') password: NgModel; 

  validate(control: AbstractControl): { [key: string]: any } {
    return this.password ? repassValidator(this.password.value)(control)
      : null;
  }
}
