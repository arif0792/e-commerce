
import { Directive, Input, forwardRef } from "@angular/core";
import { NG_ASYNC_VALIDATORS, Validator } from "@angular/forms";
import { AccountProvider } from "../../providers/account/account";
import { User } from "../../class/User";


@Directive({
  selector: "[asyncUsernameValidator]",
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => UsernameCheckDirective), multi: true
    }
  ]
})


export class UsernameCheckDirective implements Validator {
  @Input() defaultValue: string;

  constructor(public accountProvider: AccountProvider) { }

  validate(username) {
    return new Promise((resolve, reject) => {
      if (username.value
        && username.value != this.defaultValue) {
        let loginItem = new User(null, null, null, null, null, username.value, null, null, null, null, null);
        this.accountProvider.userNameCheck(loginItem)
          .subscribe(data => {
            console.log(data);
            if (!data.success) {
              resolve({ async: 'true' });
            } else {
              resolve(null);
            }
          },
          (error: any) => console.log(error)
          );
      } else {
        resolve(null);
      }
    });
  }

}
