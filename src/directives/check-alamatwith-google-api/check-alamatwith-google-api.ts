import { Directive, forwardRef, Input } from "@angular/core";
import { NG_ASYNC_VALIDATORS, Validator } from "@angular/forms";
import { GoogleApiProvider } from "../../providers/google-api/google-api";
import { Address } from "../../class/Address";
import { AvailablePostCode } from "../../assets/config/kodepos";



@Directive({
  selector: "[check-alamatwith-google-api]",
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => CheckAlamatwithGoogleApiDirective), multi: true
    }
  ]
})

export class CheckAlamatwithGoogleApiDirective implements Validator {
  @Input() addressParam: Address;
  availablePostCode = AvailablePostCode.key;

  constructor(public googleAPIProvider: GoogleApiProvider) { }

  validate(postalCode) {
    return new Promise((resolve, reject) => {
      let done = false;
      console.log(JSON.stringify(this.addressParam));
      if (postalCode.value && this.addressParam) {
        this.googleAPIProvider.postGeoCodingInquiry(this.addressParam.address)
          .subscribe(data => {
            console.log(data);
            for (let result of data.results) {
              let kelurahanName = "";
              for (let addrCompo of result.address_components) {
                addrCompo.types.forEach(data => {
                  if (data == "administrative_area_level_4") {
                    kelurahanName = addrCompo.long_name;
                  }
                  if (data == "postal_code"
                    && addrCompo.long_name == postalCode.value) {
                    done = true;
                    if (this.availablePostCode.find(x => x == postalCode.value)) {
                      this.addressParam.kelurahan = kelurahanName;
                      resolve(null);
                    } else {
                      this.addressParam.kelurahan = " - ";
                      resolve({ notavailable: 'true' });
                    }
                  }
                });
                if (done) {
                  break;
                }
              }
              if (done) {
                break;
              }
            }
            if (!done) {
              this.addressParam.kelurahan = " - ";
              resolve({ async: 'true' });
            }
          },
          (error: any) => console.log(error)
          );
      } else {
        this.addressParam.kelurahan = " - ";
        resolve(null);
      }
    });
  }

}
