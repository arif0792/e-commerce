
import { Directive, Input, forwardRef } from "@angular/core";
import { NG_ASYNC_VALIDATORS, Validator } from "@angular/forms";
import { AccountProvider } from "../../providers/account/account";
import { User } from "../../class/User";


@Directive({
  selector: "[asyncEmailValidator]",
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => EmailCheckDirective), multi: true
    }
  ]
})


export class EmailCheckDirective implements Validator {
  @Input() defaultValue: string;

  constructor(public accountProvider: AccountProvider) { }

  validate(email) {
    return new Promise((resolve, reject) => {
      if (email.value
        && email.value != this.defaultValue) {
        let loginItem = new User(null, null, null, email.value, null, null, null, null, null, null, null);
        this.accountProvider.emailCheck(loginItem)
          .subscribe(data => {
            console.log(data);
            if (!data.success) {
              resolve({ async: 'true' });
            } else {
              resolve(null);
            }
          },
          (error: any) => console.log(error)
          );
      } else {
        resolve(null);
      }
    });
  }

}
