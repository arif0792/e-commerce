
import { Directive, Input, forwardRef } from "@angular/core";
import { NG_ASYNC_VALIDATORS, Validator } from "@angular/forms";
import { AccountProvider } from "../../providers/account/account";
import { User } from "../../class/User";


@Directive({
  selector: "[asyncPhonenumberValidator]",
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => PhonenumberCheckDirective), multi: true
    }
  ]
})


export class PhonenumberCheckDirective implements Validator {
  @Input() defaultValue: string;

  constructor(public accountProvider: AccountProvider) { }

  validate(phonenumber) {
    return new Promise((resolve, reject) => {
      if (phonenumber.value
        && phonenumber.value != this.defaultValue) {
        let loginItem = new User(null, null, null, null, phonenumber.value, null, null, null, null, null, null);
        this.accountProvider.phoneNumberCheck(loginItem)
          .subscribe(data => {
            console.log(data);
            if (!data.success) {
              resolve({ async: 'true' });
            } else {
              resolve(null);
            }
          },
          (error: any) => console.log(error)
          );
      } else {
        resolve(null);
      }
    });
  }

}
