import { Directive, Host, Self, Optional } from '@angular/core';
import { TextInput } from 'ionic-angular';

@Directive({
  selector: '[integer]',
  host: {
    '(input)': 'onInputChange($event)'
  }
})
export class IntegerDirective {

  pattern: string;
  nativeElement: HTMLInputElement;

  constructor(
    @Host() @Self() @Optional() public hostInput: TextInput,
  ) { }

  ngAfterViewInit() {
    // TODO: Sanity-check that the directive was actually added to an input element.
    // It is not valid for anything else.
    // console.log('after init: ', this.hostInput._native);
    this.nativeElement = this.hostInput._native.nativeElement as HTMLInputElement;
    this.update({ target: this.nativeElement });
  }

  onInputChange(e) {
    this.update(e);
  }

  update(e) {
    try {
      let value: string = e.target.value,
        caret = e.target.selectionStart;

      for (let i = 0; i < value.length; i++) {
        if (isNaN(parseInt(value.charAt(i)))) {
          value = value.substring(0, i) + value.substring(i + 1, value.length);
        }
      }

      this.hostInput.setValue(value == "" ? "" : parseInt(value));
      if (caret < value.length) {
        e.target.setSelectionRange(caret, caret);
      }
    } catch (ex) {
      console.error(ex.message);
    }

  }

}