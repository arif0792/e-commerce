import { Directive, Attribute, Host, Self, Optional } from '@angular/core';
import { TextInput } from 'ionic-angular';

@Directive({
  selector: '[mask]',
  host: {
    '(input)': 'onInputChange($event)'
  }
})
export class MaskingDirective {

  pattern: string;
  nativeElement: HTMLInputElement;

  constructor(
    @Attribute('mask') pattern: string,
    @Host() @Self() @Optional() public hostInput: TextInput,
  ) {
    this.pattern = pattern;
  }

  ngAfterViewInit() {
    // TODO: Sanity-check that the directive was actually added to an input element.
    // It is not valid for anything else.
    // console.log('after init: ', this.hostInput._native);
    this.nativeElement = this.hostInput._native.nativeElement as HTMLInputElement;
    this.updateMask({ target: this.nativeElement });
  }

  onInputChange(e) {
    this.updateMask(e);
  }

  updateMask(e) {
    try {
      let value: string = e.target.value,
        caret = e.target.selectionStart,
        pattern = this.pattern,
        reserve = pattern.replace(/\*/, 'g'),
        applied = '',
        ordinal = 0;

      if (e.keyCode === 8 || e.key === 'Backspace' || e.keyCode === 46 || e.key === 'Delete') {
        if (value.length) {
          // remove all trailing formatting
          while (value.length && pattern[value.length] && pattern[value.length] !== '*') {
            value = value.substring(0, value.length - 1);
          }
          // remove all leading formatting to restore placeholder
          if (pattern.substring(0, value.length).indexOf('*') < 0) {
            value = value.substring(0, value.length - 1);
          }
        }
      }


      for (let i = 0; i < value.length; i++) {
        if (pattern[i] == '*' && isNaN(parseInt(value.charAt(i)))) {
          value = value.substring(0, i)+value.substring(i+1,value.length);
        }
      }


      // apply mask characters
      for (var i = 0; i < value.length; i++) {
        // enforce pattern limit
        if (i < pattern.length) {
          // match mask
          if (value[i] === pattern[ordinal]) {
            applied += value[i];
            ordinal++;
          } else if (reserve.indexOf(value[i]) > -1) {
            // skip other reserved characters
          } else {
            // apply leading formatting
            while (ordinal < pattern.length && pattern[ordinal] !== '*') {
              applied += pattern[ordinal];
              ordinal++;
            }
            applied += value[i];
            ordinal++;
            // apply trailing formatting
            while (ordinal < pattern.length && pattern[ordinal] !== '*') {
              applied += pattern[ordinal];
              ordinal++;
            }
          }
        }
      }
      // e.target.value = applied;
      this.hostInput.setValue(applied);
      if (caret < value.length) {
        e.target.setSelectionRange(caret, caret);
      }
    } catch (ex) {
      console.error(ex.message);
    }

  }

}