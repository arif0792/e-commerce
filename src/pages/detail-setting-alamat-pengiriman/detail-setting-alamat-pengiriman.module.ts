import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailSettingAlamatPengirimanPage } from './detail-setting-alamat-pengiriman';

@NgModule({
  declarations: [
    DetailSettingAlamatPengirimanPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailSettingAlamatPengirimanPage),
  ],
})
export class DetailSettingAlamatPengirimanPageModule {}
