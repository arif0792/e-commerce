import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Order } from '../../class/Order';
import { Address } from '../../class/Address';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Storage } from '@ionic/storage';
import { User } from '../../class/User';
import { ProductProvider } from '../../providers/product/product';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

/**
 * Generated class for the DetailSettingAlamatPengirimanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-setting-alamat-pengiriman',
  templateUrl: 'detail-setting-alamat-pengiriman.html',
})
export class DetailSettingAlamatPengirimanPage {
  apiUrl: string;
  order: Order;
  userData: User;
  addresses: Address[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public alertCtrl: AlertController, public productProvider: ProductProvider, public loadingCtrl: LoadingController) {
    this.order = this.navParams.get('order');
    console.log(this.order);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailSettingAlamatPengirimanPage');
  }

  ionViewWillEnter() {
    this.storage.get('userData').then(
      data => {
        this.userData = JSON.parse(data);
        this.addresses = this.userData.addresses;
      }
    );
  }

  goToAccountAlamat() {
    this.navCtrl.push('SettingAlamatPage');
  }

  changeToInt(e): number {
    console.log(e);
    return +e;
  }

  onSave() {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    loading.present();

    console.log(this.order.products)
    this.productProvider.saveProducts(this.order.products).subscribe(
      data => {
        console.log(data);
        this.order.products = data;
        for (let obj of this.userData.orders) {
          if (obj.ID == this.order.ID) {
            obj.products = this.order.products;
            break;
          }
        }
        console.log(this.userData.orders);
        this.storage.set('userData', JSON.stringify(this.userData));
        let alert = this.alertCtrl.create({
          title: 'Berhasil',
          message: 'Data berhasil disimpan',
          buttons: ['Ok']
        });
        loading.dismiss();
        alert.present();
      },
      (error: any) => {this.popupError(error); loading.dismiss(); }
    );
  }

  onChangeOption(e: any): number{
    return e.value == 'POTONG' ? 100000 : 150000
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
