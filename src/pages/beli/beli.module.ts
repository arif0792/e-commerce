import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeliPage } from './beli';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    BeliPage
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(BeliPage),
  ],
})
export class BeliPageModule {}
