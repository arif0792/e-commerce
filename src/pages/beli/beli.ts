import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Keyboard } from 'ionic-angular';
import { Product } from '../../class/Product';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Order } from '../../class/Order';
import { ItemProvider } from '../../providers/item/item';
import { SettingsProvider } from '../../providers/settings/settings';
import { Address } from '../../class/Address';
import { Status } from '../../enum/status';
import { User } from '../../class/User';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { Events } from 'ionic-angular/util/events';
import { ItemDetail } from '../../class/ItemDetail';


/**
 * Generated class for the BeliKambingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-beli-kambing',
  templateUrl: 'beli.html'
})
export class BeliPage {
  itemImages: ByteString[] = [];
  settingLebaranMonth: number;
  settingRefund: number;
  hasLoggedIn: boolean = false;
  userData: User;
  order: Order;
  items: ItemDetail[] = [];
  installments: Array<{ name: string, value: number }> = [];
  dataProducts: Array<{ item: ItemDetail, qty: number }> = [];
  errorMsg: string;
  addresses: Address[] = [];
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public itemProvider: ItemProvider,
    public settingsProvider: SettingsProvider, public storage: Storage, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public event: Events, public keyboard: Keyboard) {


  }

  //get Parameter items
  getItems(category: string): Promise<ItemDetail[]> {
    return new Promise((resolve, reject) => {
      this.itemProvider.getItemsByCategory(category).subscribe(
        items => {
          console.log(items);
          if (items[0].itemPictureIds) {
            let ids = JSON.parse(items[0].itemPictureIds);
            this.itemProvider.getItemPictures(ids.id).subscribe(
              data => {
                console.log(data);
                this.itemImages = [];
                for (let pic of data.data) {
                  this.itemImages.push("data:image/jpeg;base64,"+pic.data);
                }
                console.log(this.itemImages);
              },
              (error: any) => { this.popupError(error); this.loading.dismiss(); }
            );
          }
          resolve(items);
        },
        (error: any) => { this.popupError(error); this.loading.dismiss(); }
      );
    });
  }

  //get Parameter Installment lebaran tahunan
  getInstallment() {
    this.settingsProvider.getSettingLebaranMonth().subscribe(
      (lebaran: number) => {
        this.settingsProvider.getSettingDeadlineCancel().subscribe(
          (refund: number) => {
            this.settingRefund = refund;
            this.settingLebaranMonth = lebaran;
            this.generateInstallments(lebaran);
            this.loading.dismiss();
          }
        );
      }
    );
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  generateInstallments(month: number) {
    console.log('installment month param =' + month);
    let past = false;
    let dateNow = new Date().getMonth() + 1;
    month < dateNow ? past = true : past = false;
    if (past) {
      let jumlah = month - dateNow + 12;
      for (let i = 1; i < jumlah; i++) {
        this.installments.push({ name: i + " Bulan", value: i });
      }
    } else {
      let jumlah = month - dateNow;
      for (let i = 1; i < jumlah; i++) {
        this.installments.push({ name: i + " Bulan", value: i });
      }
    }
  }

  getTotal() {
    let total = 0;
    for (let dataProduct of this.dataProducts) {
      total += dataProduct.item && dataProduct.qty ? (dataProduct.item.itemPrice + dataProduct.item.itemAdminPrice) * dataProduct.qty : 0;
    }
    return total;
  }

  getAngsuran() {
    return this.order.totalInstallment != null && this.getTotal() !== 0 ? this.getTotal() / this.order.totalInstallment : 0;
  }

  changeToInt(e: any): number {
    return +e;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BeliKambingPage');
  }

  ionViewWillEnter() {

    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    //
    console.log('ionViewWillEnter');
    //refresh get alamat
    this.storage.get('userData').then(
      data => {
        let dataObj = JSON.parse(data);
        this.userData = dataObj;
        console.log(dataObj);
        this.userData ? this.addresses = this.userData.addresses : null;
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
    this.storage.get('hasLoggedIn').then(
      data => {
        this.hasLoggedIn = JSON.parse(data);
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
    // data2 load

    if (!this.order) {
      this.getItems('KAMBING').then(
        data => {
          this.items = data;
          this.getInstallment();
        }
      );
      this.order = new Order(null, new Date(), [], null, null, null, null, null, null, null, null, null, null, null, null, null);
    } else {
      this.loading.dismiss();
    }
    if (this.dataProducts.length == 0) {
      this.newDataProduct();
    }
    console.log('new ->' + this.order);
  }

  newDataProduct() {
    this.dataProducts.push({ item: null, qty: null });
  }

  syncData() {
    this.order.products = [];
    this.order.haveCancelDeadline = false;
    for (let obj of this.dataProducts) {
      console.log(obj);
      if (obj.qty && obj.item) {
        for (let i = 0; i < obj.qty; i++) {
          this.order.products.push(new Product(null, new Date(), null, 0, null, this.order.ID, obj.item.itemName, obj.item.itemPrice, obj.item.itemAdminPrice, 100000, obj.item.itemCategory, "POTONG"));
        }
      }
      //haveCancelDeadline
      if (obj.item && obj.item.haveCancelDeadline) {
        this.order.haveCancelDeadline = true;
        this.order.deadlineCancelMonth = this.settingRefund;
      } else if (obj.item) {
        this.order.haveCancelDeadline = false;
      }
      if (obj.item && obj.item.itemCategory == 'KAMBING') {
        this.order.lebaranMonth = this.settingLebaranMonth;
      }
    }
  }

  onChangeOption(e: any): number {
    return e.value == 'POTONG' ? 100000 : 150000
  }

  goToAccountAlamat() {
    if (this.hasLoggedIn) {
      this.navCtrl.push('SettingAlamatPage');
    } else {
      let alert = this.alertCtrl.create({
        title: 'Belum Masuk Akun',
        message: 'Anda harus masuk akun anda terlebih dahulu.',
        buttons: [
          {
            text: 'Tidak',
            role: 'cancel',
            handler: () => {
              console.log('Tidak clicked');
            }
          },
          {
            text: 'Masuk',
            handler: () => {
              this.navCtrl.push('LoginPage');
            }
          }
        ]
      });
      alert.present();
    }
  }

  onSubmit() {
    if (!this.hasLoggedIn) {
      let alert = this.alertCtrl.create({
        title: 'Belum Masuk Akun',
        message: 'Anda harus masuk akun anda terlebih dahulu.',
        buttons: [
          {
            text: 'Tidak',
            role: 'cancel',
            handler: () => {
              console.log('Tidak clicked');
            }
          },
          {
            text: 'Masuk',
            handler: () => {
              this.navCtrl.push('LoginPage');
            }
          }
        ]
      });
      alert.present();
    } else {
      this.order.totalPrice = this.getTotal();
      this.order.status = Status.WAITING_FOR_PAYMENT;
      this.order.orderDate = new Date();
      this.order.userId = this.userData.ID;
      console.log(this.order);
      this.navCtrl.push('PaymentGatewayPage', {
        order: this.order
      });
    }
  }

  onClickDelete(index: number, form: NgForm, models: NgModel[]) {
    this.dataProducts.splice(index, 1);
    console.log(this.dataProducts);
    this.syncData();
    Object.keys(form.controls).forEach(control => {
      form.controls[control].markAsUntouched();
    });
  }

  trackByIndex(index: number) {
    return index;
  }

  getCurrency(num: number): string {
    return Math.round(num).toLocaleString(undefined, { minimumFractionDigits: 0 }).replace(/,/g, '.');
  }

  closeKeyboard() {
    this.keyboard.close();
  }
}
