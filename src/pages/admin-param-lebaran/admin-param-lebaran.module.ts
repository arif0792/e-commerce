import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminParamLebaranPage } from './admin-param-lebaran';

@NgModule({
  declarations: [
    AdminParamLebaranPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminParamLebaranPage),
  ],
})
export class AdminParamLebaranPageModule {}
