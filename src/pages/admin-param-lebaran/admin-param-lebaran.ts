import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/**
 * Generated class for the AdminParamLebaranPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-param-lebaran',
  templateUrl: 'admin-param-lebaran.html',
})
export class AdminParamLebaranPage {
  paramLebaran: any;
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public settingProvider: SettingsProvider,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminParamLebaranPage');
  }

  ionViewWillEnter() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.settingProvider.getSettingLebaranMonth().subscribe(
      data => {
        this.paramLebaran = data.toString();
        this.loading.dismiss();
      }
    );
  }

  onSubmit() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.settingProvider.setSettingLebaranMonth(this.paramLebaran.toString()).subscribe(
      () => {
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Berhasil',
          message: 'Berhasil',
          buttons: ['Ok']
        });
        alert.present();
      },
      (error: any) => {
        this.popupError(error);
        this.loading.dismiss();
      }
    );
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
