import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { InAppBrowser, InAppBrowserEvent } from '@ionic-native/in-app-browser';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

/**
 * Generated class for the ModalPaymentgatewayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'modal-page-payment-gateway',
  template: `
  <ion-header>
  </ion-header>
  <ion-content>
  </ion-content>
  `
})
export class ModalPaymentgatewayPage {
  url;
  constructor(public navParams: NavParams, public viewCtrl: ViewController,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController, public inAppBrowser: InAppBrowser) {
    this.url = this.navParams.get('url');
    let payment = inAppBrowser.create(this.url, "_blank", "location=no,toolbar=no,EnableViewPortScale=yes,closebuttoncaption=Done");
    payment.on('loadstop')
      .subscribe(
      (event: InAppBrowserEvent) => {
        if (event.url.includes("https://gentle-thicket-39624.herokuapp.com/")) {
          let resultCode = event.url.substring(event.url.indexOf("resultCode") + 11, event.url.indexOf("resultCode") + 13);
          let refID = event.url.substring(event.url.indexOf("reference") + 10, event.url.length);
          let mapData = new Map<string, string>();
          mapData.set("resultCode", resultCode);
          mapData.set("refID", refID);
          this.viewCtrl.dismiss(mapData);
          payment.close();
        }
      },
      err => {
        console.log("InAppBrowser Loadstop Event Error: " + err);
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPaymentgatewayPage');
  }

} 
