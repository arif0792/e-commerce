import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalPaymentgatewayPage} from './modal-paymentgateway';

@NgModule({
  declarations: [
    ModalPaymentgatewayPage
  ],
  imports: [
    IonicPageModule.forChild(ModalPaymentgatewayPage),
  ],
})
export class ModalPaymentgatewayPageModule {}
