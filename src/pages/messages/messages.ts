import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MessageProvider } from '../../providers/message/message';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { User } from '../../class/User';
import { Events } from 'ionic-angular/util/events';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';

/**
 * Generated class for the MessagesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {
  @ViewChild('header') header: ElementRef;
  messages: Array<{ message: { ID: number, from: string, data: string, userId?: number }, read: boolean }> = [];
  userData: User;
  loading: Loading;
  topLoading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public messageProvider: MessageProvider,
    public alertCtrl: AlertController, public event: Events, public loadingCtrl: LoadingController) {
  }

  ionViewWillEnter() {
    this.topLoading = true;

    // this.event.subscribe('resume', (data) => {
    //   window.location.reload();
    // });

    this.storage.get('userData').then(
      data => {
        this.userData = JSON.parse(data)
        //preload msg
        let badgeCount = 0, readCount = 0;
        for (let msg of this.userData.messages) {
          let obj: { message: { ID: number, from: string, data: string, userId?: number }, read: boolean } = { message: null, read: false };
          obj.message = msg;
          for (let msgRead of this.userData.reads) {
            if (msgRead.messageId == msg.ID
              && msgRead.read) {
              obj.read = true;
              readCount += 1;
              break;
            }
          }
          this.messages.push(obj);
        }
        badgeCount = this.userData.messages.length - readCount;
        this.event.publish('messagesBadge', badgeCount);
        this.storage.set('messagesBadge', badgeCount);
        //
        this.refreshMsg();
      },
      (error: any) => { this.popupError(error); this.topLoading = false; }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
  }

  goToDetail(msg: any) {
    console.log(msg);
    if (!msg.read) {
      this.setMsgAsRead(msg);
    }
    this.navCtrl.push('MessageDetailPage', {
      msg: msg.message
    });
  }

  setMsgAsRead(msg: any) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.messageProvider.postRead({ read: true, messageId: msg.message.ID, userId: this.userData.ID }).subscribe(
      data => {
        console.log(data);
        this.refreshMsgsForRead(data.data);
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  refreshMsgsForRead(newRead: any) {
    this.userData.reads.push(newRead);
    this.refreshMsg();
  }

  refreshMsg() {
    this.messageProvider.getMessages(this.userData.ID.toString()).subscribe(
      msgs => {
        this.userData.messages = msgs.data;
        this.messageProvider.getReads(this.userData.ID.toString()).subscribe(
          rds => {
            this.userData.reads = rds.data;
            let badgeCount = 0, readCount = 0;
            this.messages = [];
            for (let msg of this.userData.messages) {
              let obj: { message: { ID: number, from: string, data: string, userId?: number }, read: boolean } = { message: null, read: false };
              obj.message = msg;
              for (let msgRead of this.userData.reads) {
                if (msgRead.messageId == msg.ID
                  && msgRead.read) {
                  obj.read = true;
                  readCount += 1;
                  break;
                }
              }
              this.messages.push(obj);
            }
            badgeCount = this.userData.messages.length - readCount;
            this.event.publish('messagesBadge', badgeCount);
            this.storage.set('messagesBadge', badgeCount);
            this.storage.set('userData', JSON.stringify(<User>this.userData));
            this.topLoading = false;
          }
        );
      }
    );
  }

  getTop(): number{
    return this.header.nativeElement.offsetHeight;
  }
}
