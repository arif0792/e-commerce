import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';
import { ItemDetail } from '../../class/ItemDetail';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-admin-param-items',
  templateUrl: 'admin-param-items.html',
})
export class AdminParamItemsPage {

  mapItems: Map<string, Array<any>> = new Map<string, Array<any>>();
  mapKeys;
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public itemProvider: ItemProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminParamItemsPage');
  }

  ionViewWillEnter() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.mapItems = new Map<string, Array<any>>();
    this.itemProvider.getAllItems().subscribe(
      data => {
        for (let obj of data) {
          if (this.mapItems.get(obj.itemCategory)) {
            this.mapItems.get(obj.itemCategory).push(obj);
          } else {
            this.mapItems.set(obj.itemCategory, []);
            this.mapItems.get(obj.itemCategory).push(obj);
          }
        }
        this.mapKeys = Array.from(this.mapItems.keys());
        this.loading.dismiss();
      }
    );
  }

  onChange(map: string, i: number, event: any) {
    let items = this.mapItems.get(map);
    items[i].itemPrice = parseInt(event.value);
  }

  onSubmit() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    let itemDetails: ItemDetail[] = [];
    for (let key of this.mapKeys) {
      for (let obj of this.mapItems.get(key)) {
        itemDetails.push(obj);
      }
    }
    this.itemProvider.saveItems(itemDetails).subscribe(
      data => {
        console.log();
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Berhasil',
          message: 'Berhasil',
          buttons: ['Ok']
        });
        alert.present();
      },
      (error: any) => {
        this.popupError(error);
        this.loading.dismiss();
      }
    );
  }

  deleteItem(map: string, i: number) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.itemProvider.deleteItem(this.mapItems.get(map)[i].ID).subscribe(
      data => {
        console.log(data);
        this.loading.dismiss();
        this.ionViewWillEnter();
      },
      (error: any) => {
        this.popupError(error);
        this.loading.dismiss();
      }
    );
  }

  newItem() {
    this.navCtrl.push('AdminParamNewitemPage', {
      mapItems: this.mapItems
    });
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  goToImagesSetting(key: string) {
    this.navCtrl.push('AdminParamPicsItemsPage', {
      items: this.mapItems.get(key)
    });
  }

}
