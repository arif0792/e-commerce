import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminParamItemsPage } from './admin-param-items';

@NgModule({
  declarations: [
    AdminParamItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminParamItemsPage),
  ],
})
export class AdminParamItemsPageModule {}
