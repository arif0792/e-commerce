import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAdminSendMsgPage } from './modal-admin-send-msg';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    ModalAdminSendMsgPage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(ModalAdminSendMsgPage),
  ],
})
export class ModalAdminSendMsgPageModule {}
