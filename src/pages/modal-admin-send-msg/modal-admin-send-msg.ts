import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { MessageProvider } from '../../providers/message/message';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'modal-page-admin-dashboard-user',
  template: `
  <ion-header>
	<ion-navbar>
		<ion-buttons>
			<button ion-button large (click)="cancel()">
				<ion-icon large name="close"></ion-icon>
			</button>
		</ion-buttons>
		<ion-title>Send Message</ion-title>
	</ion-navbar>
</ion-header>
<ion-content>
  <ion-list>
  <form #formMsg="ngForm" (ngSubmit)="submit()">
    <ion-item>
      <p item-content>
        Full Name :
      </p>
      <ion-input text-end item-end [disabled]="true" [(ngModel)]="receiverFullName" name="receiverFullName">
      </ion-input>
    </ion-item>
    <ion-item>
    <p item-center>
      Message (Max. 255) :
      </p>
    </ion-item>
    <ion-item>
    <ion-textarea autosize maxLength="255" text-end item-end required [(ngModel)]="msgs" name="msgs">
    </ion-textarea>
    </ion-item>
    <br/>
    <ion-buttons text-center>
      <button color="secondary" block ion-button type="submit" [disabled]="!formMsg.valid">
        Send
      </button>
    </ion-buttons>
  </form>
  </ion-list>
  </ion-content>
    `
})


export class ModalAdminSendMsgPage {
  receiverId: number;
  receiverFullName: string;
  msgs: string;
  constructor(public navParams: NavParams, public viewCtrl: ViewController, public messageProvider: MessageProvider,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.receiverId = this.navParams.get('userId');
    this.receiverFullName = this.navParams.get('userFullName');
  }

  submit() {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    loading.present();

    this.messageProvider.postMessage({ from: "Admin", data: this.msgs, userId: this.receiverId }).subscribe(
      data => {
        console.log(data);
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Berhasil',
          message: 'Telah berhasil mengirim pesan.',
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.cancel();
              }
            },
          ]
        });
        alert.present();
      },
      (error: any) => {this.popupError(error); loading.dismiss(); }
    );
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }


}
