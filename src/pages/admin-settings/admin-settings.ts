import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AdminSettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-settings',
  templateUrl: 'admin-settings.html',
})
export class AdminSettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminSettingsPage');
  }

  forwardToParamLebaran(){
    this.navCtrl.push('AdminParamLebaranPage');
  }

  forwardToParamRefund(){
    this.navCtrl.push('AdminParamRefundPage');
  }

  forwardToParamItems(){
    this.navCtrl.push('AdminParamItemsPage');
  }

  forwardToParamHargaAdmin(){
    this.navCtrl.push('AdminHargaAdminPage');
  }

  forwardToParamBanner(){
    this.navCtrl.push('AdminParamPicsBannerPage');
  }

}
