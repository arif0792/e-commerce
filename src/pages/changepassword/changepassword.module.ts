import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangepasswordPage } from './changepassword';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    ChangepasswordPage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(ChangepasswordPage),
  ],
})
export class ChangepasswordPageModule {}
