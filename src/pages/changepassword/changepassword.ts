import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../class/User';
import { AccountProvider } from '../../providers/account/account';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ChangepasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {
  loading: Loading;
  userData: User;
  oldPassword: string;
  newpassword: string;
  renewpassword: string;
  errorMsg: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public accountProvider: AccountProvider,
    public alertCtrl: AlertController, public loadingCtrl: LoadingController, public firebase: AngularFireAuth, public storage: Storage) {
    this.userData = this.navParams.get('userData');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepasswordPage');
  }

  onSubmit() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.firebase.auth.signInWithEmailAndPassword(this.userData.email, this.oldPassword).then(
      () => {
        this.userData.password = this.newpassword;
        this.accountProvider.postChangePasswordAccount(this.userData).subscribe(
          data => {
            this.firebase.auth.signInWithEmailAndPassword(data.data.email, data.data.password).then(
              () => {
                this.storage.set('userData', JSON.stringify(<User>data.data));
                this.loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: 'Berhasil',
                  message: "Password anda berhasil di rubah",
                  buttons: ['OK']
                });
                alert.present();
                this.oldPassword = "";
                this.newpassword = "";
                this.renewpassword = "";
              },
              (error: any) => { this.popupError(error); this.loading.dismiss(); }
            );
          },
          (error: any) => { this.popupError(error); this.loading.dismiss(); }
        );
        this.errorMsg = "";

      },
      (error: any) => {
        this.loading.dismiss();
        this.errorMsg = "Password lama anda salah";
      }
    );
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }



}
