import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminBroadcastPage } from './admin-broadcast';

@NgModule({
  declarations: [
    AdminBroadcastPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminBroadcastPage),
  ],
})
export class AdminBroadcastPageModule {}
