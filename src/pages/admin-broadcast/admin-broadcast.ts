import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { MessageProvider } from '../../providers/message/message';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

/**
 * Generated class for the AdminBroadcastPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-broadcast',
  templateUrl: 'admin-broadcast.html',
})
export class AdminBroadcastPage {

  msgs: string;

  constructor(public navParams: NavParams, public messageProvider: MessageProvider,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
  }

  submit() {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    loading.present();

    this.messageProvider.postMessage({ from: "Admin", data: this.msgs, userId: 0 }).subscribe(
      data => {
        console.log(data);
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Berhasil',
          message: 'Broadcast telah dikirim.',
          buttons: [
            {
              text: 'OK',
              handler: () => {
                this.msgs = "";
              }
            },
          ]
        });
        alert.present();
      },
      (error: any) => {this.popupError(error); loading.dismiss(); }
    );
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
