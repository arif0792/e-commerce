import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Order } from '../../class/Order';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { User } from '../../class/User';
import { Storage } from '@ionic/storage';
import { OrderProvider } from '../../providers/order/order';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { PRODUCT_STATUS } from '../../enum/productStatus';
import { Events } from 'ionic-angular/util/events';
import { MessageProvider } from '../../providers/message/message';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  @ViewChild('header') header: ElementRef;
  productStatus = PRODUCT_STATUS;
  loading: Loading;
  apiUrl: string;
  userData: User;
  orders: Order[] = [];
  topLoading = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public event: Events,
    public orderProvider: OrderProvider, public alrtCtrl: AlertController, public loadingCtrl: LoadingController, public messageProvider: MessageProvider) {

    // this.event.subscribe('resume', (data) => {
    //   window.location.reload();
    // });

  }

  getOrders() {
    this.topLoading = true;

    this.storage.get('userData').then(
      data => {
        this.userData = JSON.parse(data);
        //preloading data awal untuk dimunculin biar ga kosong
        this.userData.orders ? this.userData.orders.forEach(element => {
          let obj = new Order(element.ID, element.CreatedAt, element.products, element.totalInstallment, element.totalPrice, element.paymentDetails, element.status, element.orderDate, element.isRefund, element.userId, element.canceledDate, element.completedDate, element.haveCancelDeadline, element.isActive, element.deadlineCancelMonth, element.lebaranMonth);
          this.orders.push(obj);
        }) : null;
        //
        this.orderProvider.getOrders(this.userData.ID.toString()).subscribe(
          data => {
            // let dataObj = JSON.parse(data);
            this.userData.orders = data;
            console.log(this.userData);
            this.orders = [];
            this.userData.orders ? this.userData.orders.forEach(element => {
              let obj = new Order(element.ID, element.CreatedAt, element.products, element.totalInstallment, element.totalPrice, element.paymentDetails, element.status, element.orderDate, element.isRefund, element.userId, element.canceledDate, element.completedDate, element.haveCancelDeadline, element.isActive, element.deadlineCancelMonth, element.lebaranMonth);
              this.orders.push(obj);
            }) : null;
            this.storage.set('userData', JSON.stringify(this.userData)).then(
              () => {
                this.topLoading = false;
              }
            );
          },
          error => { this.popupError(error); this.topLoading = false; }
        );
      }
    );
  }

  ionViewWillEnter() {
    this.getOrders();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  setPengiriman(obj: Order) {
    this.navCtrl.push('DetailSettingAlamatPengirimanPage', {
      order: obj
    });
  }

  bayarCicilan(obj: Order) {
    this.navCtrl.push('PaymentGatewayPage', {
      order: obj
    });
  }

  cancelOrder(order: Order) {
    let alert = this.alrtCtrl.create({
      title: 'Cancel Order',
      message: 'Apakah anda yakin ingin cancel order ini?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            console.log('Tidak clicked');
          }
        },
        {
          text: 'Yakin',
          handler: () => {
            console.log('Yakin clicked');
            //
            this.loading = this.loadingCtrl.create({
              content: 'Please Wait',
              spinner: 'dots'
            });

            this.loading.present();
            let dateNow = new Date();
            let pastDeadlineCancel = false;
            let deadlineDate: Date;
            this.getDeadlineDate(order.lebaranMonth, order.deadlineCancelMonth).then(
              data => {
                deadlineDate = data;
                console.log(data);
                if (dateNow > deadlineDate) {
                  pastDeadlineCancel = true;
                }
                //
                if (pastDeadlineCancel) {
                  this.loading.dismiss();
                  //more alert deadline cancel if have
                  let alert = this.alrtCtrl.create({
                    title: 'Cancel Order',
                    message: 'Order ini melebihi batas deadline cancel, biaya admin tidak akan direfund, Apakah anda yakin ingin cancel order ini?',
                    buttons: [
                      {
                        text: 'Tidak',
                        role: 'cancel',
                        handler: () => {
                          console.log('Tidak clicked');
                        }
                      },
                      {
                        text: 'Yakin',
                        handler: () => {
                          console.log('Yakin clicked');
                          this.loading = this.loadingCtrl.create({
                            content: 'Please Wait',
                            spinner: 'dots'
                          });

                          this.loading.present();
                          this.orderProvider.postCancelOrder(order)
                            .subscribe(
                              data => this.refreshDataCancel(data.data),
                              (error: any) => { this.popupError(error); this.loading.dismiss(); }
                            );
                        }
                      }
                    ]
                  });
                  alert.present();
                } else {
                  this.orderProvider.postCancelOrder(order)
                    .subscribe(
                      data => this.refreshDataCancel(data.data),
                      (error: any) => { this.popupError(error); this.loading.dismiss(); }
                    );
                }
              }
            );
          }
        }
      ]
    });
    alert.present();
  }

  popupError(obj: any) {
    let alert = this.alrtCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  refreshDataCancel(orderCanceled: Order) {
    this.postMessage({ from: "Admin", data: "Order #" + orderCanceled.ID.toString() + " telah berhasil di cancel.", userId: orderCanceled.userId }).then(
      () => {
        for (let ord of this.orders) {
          if (ord.ID == orderCanceled.ID) {
            ord = orderCanceled
            break;
          }
        }
        this.userData.orders = this.orders;
        this.storage.set('userData', JSON.stringify(this.userData));
        this.loading.dismiss();
      }
    );
  }

  postMessage(msg: any) {
    return new Promise((resolve, reject) => {
      this.messageProvider.postMessage(msg).subscribe(
        data => {
          console.log(data);
          resolve();
        },
        (error: any) => {
          this.popupError(error);
          this.loading.dismiss();
        }
      );
    });
  }

  getDeadlineDate(lebaranSetting: number, refundSetting: number): Promise<Date> {
    return new Promise((resolve, reject) => {
      let dateLebaran = new Date();
      dateLebaran.setMonth(lebaranSetting - refundSetting);
      resolve(dateLebaran);
    });
  }

  getCurrency(num: number): string {
    return num ? Math.round(num).toLocaleString(undefined, { minimumFractionDigits: 0 }).replace(/,/g, '.') : '0';
  }

  getAngsuran(order: Order) {
    return (order.totalPrice / order.totalInstallment);
  }

  getTop(): number{
    return this.header.nativeElement.offsetHeight;
  }

}
