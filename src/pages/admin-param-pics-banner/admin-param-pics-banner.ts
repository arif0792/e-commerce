import { Component } from '@angular/core';
import { NavController, IonicPage, NavParams, AlertController, Loading, LoadingController } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';
import { SettingsProvider } from '../../providers/settings/settings';
import { Camera, CameraOptions } from '@ionic-native/camera';

/**
 * Generated class for the AdminParamPicsBannerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-param-pics-banner',
  templateUrl: 'admin-param-pics-banner.html',
})
export class AdminParamPicsBannerPage {
  loading: Loading;
  banners: Array<{ ID: number, data: ByteString }> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    public itemProvider: ItemProvider, public settingProvider: SettingsProvider, public camera: Camera, public loadingCtrl: LoadingController) {
    this.settingProvider.getSettingBannerPicIds().subscribe(
      data => {
        let ids = JSON.parse(data);
        this.itemProvider.getItemPictures(ids.id).subscribe(
          data => {
            console.log(data);
            this.banners = [];
            for (let pic of data.data) {
              this.banners.push({ ID: pic.ID, data: "data:image/jpeg;base64," + pic.data });
            }
            console.log(this.banners);
          },
          (error: any) => { this.popupError(error); }
        );
      },
      (error: any) => { this.popupError(error); }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminParamPicsBannerPage');
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  setDelete(idx: number) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.itemProvider.deleteItemPicture(this.banners[idx].ID.toString()).subscribe(
      () => {
        this.banners.splice(idx, 1);
        this.loading.dismiss();
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
  }

  addPic() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then(
      (imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.loading = this.loadingCtrl.create({
          content: 'Please Wait',
          spinner: 'dots'
        });

        this.loading.present();

        let forSave: Array<{ ID: number, data: ByteString }> = [];
        forSave.push({ ID: 0, data: imageData });
        this.itemProvider.postItemPictures(forSave).subscribe(
          data => {
            console.log(data.data);
            data.data[0].data = "data:image/jpeg;base64,"+data.data[0].data;
            this.banners.push(data.data[0]);
            let tempArray: {id: string[]};
            tempArray =  {id: []};
            for(let item of this.banners){
              tempArray.id.push(item.ID.toString());
            }
            this.settingProvider.setSettingBannerPicIds(JSON.stringify(tempArray)).subscribe(
              data => {
                console.log(data);
                this.loading.dismiss();
              }
            );
          },
          (error: any) => { this.popupError(error); this.loading.dismiss(); }
        );

      }, (error: any) => {
        {
          if (error.message) {
            this.popupError(error);
          }
        }
      });
  }

}
