import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminParamPicsBannerPage } from './admin-param-pics-banner';

@NgModule({
  declarations: [
    AdminParamPicsBannerPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminParamPicsBannerPage),
  ],
})
export class AdminParamPicsBannerPageModule {}
