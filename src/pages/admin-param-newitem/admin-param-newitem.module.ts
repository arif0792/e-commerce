import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminParamNewitemPage } from './admin-param-newitem';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    AdminParamNewitemPage,
  ],
  imports: [ 
    DirectivesModule,
    IonicPageModule.forChild(AdminParamNewitemPage),
  ],
})
export class AdminParamNewitemPageModule {}
