import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemDetail } from '../../class/ItemDetail';
import { ItemProvider } from '../../providers/item/item';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';

/**
 * Generated class for the AdminParamNewitemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-param-newitem',
  templateUrl: 'admin-param-newitem.html',
})
export class AdminParamNewitemPage {
  mapItems: Map<string, Array<any>> = new Map<string, Array<any>>();
  mapKeys;
  loading: Loading;
  newItem: ItemDetail = new ItemDetail(null, null, null, null, null, null, null, null);

  constructor(public navCtrl: NavController, public navParams: NavParams, public itemProvider: ItemProvider, public loadingCtrl: LoadingController) {
    this.mapItems = this.navParams.get('mapItems');
    this.mapKeys = Array.from(this.mapItems.keys());
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminParamNewitemPage');
  }

  onSubmit() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.newItem.haveCancelDeadline = false;

    for (let key of this.mapKeys) {
      if (this.newItem.itemCategory == key) {
        this.newItem.haveCancelDeadline = this.mapItems.get(key)[0].haveCancelDeadline;
        this.newItem.itemPictureIds = this.mapItems.get(key)[0].itemPictureIds;
        this.newItem.itemAdminPrice = this.mapItems.get(key)[0].itemAdminPrice;
        break;
      }
    }

    let itemDetails: ItemDetail[] = [];
    itemDetails.push(this.newItem);
    this.itemProvider.saveItems(itemDetails).subscribe(
      data =>
        console.log(data)
    );

    this.loading.dismiss();
    this.navCtrl.pop();
  }

}
