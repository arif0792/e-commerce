import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminUserAddressesPage } from './admin-user-addresses';

@NgModule({
  declarations: [
    AdminUserAddressesPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminUserAddressesPage),
  ],
})
export class AdminUserAddressesPageModule {}
