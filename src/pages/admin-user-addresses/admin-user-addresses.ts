import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../class/User';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';

/**
 * Generated class for the AdminUserAddressesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-user-addresses',
  templateUrl: 'admin-user-addresses.html',
})
export class AdminUserAddressesPage {
  user: User;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.user = this.navParams.get('user');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminUserAddressesPage');
  }

  sendMessage() {
    let modal = this.modalCtrl.create('ModalAdminSendMsgPage', { userId: this.user.ID, userFullName: this.user.firstName + " " + this.user.lastName }, {
      cssClass: "my-modal"
    });
    modal.present();
  }

}
