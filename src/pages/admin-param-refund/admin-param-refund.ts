import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Loading } from 'ionic-angular/components/loading/loading';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { SettingsProvider } from '../../providers/settings/settings';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/**
 * Generated class for the AdminParamRefundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-param-refund',
  templateUrl: 'admin-param-refund.html',
})
export class AdminParamRefundPage {
  paramRefund: any;
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public settingProvider: SettingsProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminParamRefundPage');
  }

  ionViewWillEnter() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.settingProvider.getSettingDeadlineCancel().subscribe(
      data => {
        this.paramRefund = data.toString();
        this.loading.dismiss();
      }
    );
  }

  onSubmit() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.settingProvider.setSettingDeadlineCancel(this.paramRefund.toString()).subscribe(
      data => {
        console.log(data);
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Berhasil',
          message: 'Berhasil',
          buttons: ['Ok']
        });
        alert.present();
      },
      (error: any) => {
        this.popupError(error);
        this.loading.dismiss();
      }
    );
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }
}
