import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminParamRefundPage } from './admin-param-refund';

@NgModule({
  declarations: [
    AdminParamRefundPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminParamRefundPage),
  ],
})
export class AdminParamRefundPageModule {}
