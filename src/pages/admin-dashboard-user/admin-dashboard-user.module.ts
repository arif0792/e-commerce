import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDashboardUserPage } from './admin-dashboard-user';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    AdminDashboardUserPage
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(AdminDashboardUserPage)
  ],
})
export class AdminDashboardUserPageModule {}
