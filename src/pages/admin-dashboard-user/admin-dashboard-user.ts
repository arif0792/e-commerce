import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../class/User';
import { AccountProvider } from '../../providers/account/account';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/**
 * Generated class for the AdminDashboardUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-dashboard-user',
  templateUrl: 'admin-dashboard-user.html',
})
export class AdminDashboardUserPage {
  users: User[] = [];
  usercount: number;
  currentActiveProductsCount: number;
  loading: Loading;
  querysearch: string;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public accountProvider: AccountProvider, public loadingCtrl: LoadingController,
    public modalCtrl: ModalController, public alertCtrl: AlertController) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.querysearch = "";
    this.getUserByName(this.querysearch, 0);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminDashboardUserPage');
  }

  onEnter(value: string) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.querysearch = value;
    this.getUserByName(this.querysearch, 0);
  }

  doLoadMoreData(infiniteScroll) {
    this.getUserByName(this.querysearch, this.users.length, infiniteScroll);
  }

  getUserByName(name: string, start: number, infScroll?: any) {
    this.accountProvider.getUsersByNameCount(name).subscribe(
      data => {
        this.usercount = data.data;
        this.accountProvider.getUsersByName(name, start).subscribe(
          data => {
            if (start != 0) {
              this.users = this.users.concat(data.data);
            } else {
              this.users = data.data;
            }
            this.currentActiveProductsCount = 0;
            for (let usr of this.users) {
              for (let ord of usr.orders) {
                for (let prd of ord.products) {
                  if (ord.status === 1
                    && prd.status === 0
                  ) {
                    this.currentActiveProductsCount += 1;
                  }
                }
              }
            }
            this.loading.dismiss();
            infScroll ? infScroll.complete() : null;
          },
          (error: any) => { this.popupError(error); this.loading.dismiss(); }
        );
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
  }

  sendMessage(user: User) {
    let modal = this.modalCtrl.create('ModalAdminSendMsgPage', { userId: user.ID, userFullName: user.firstName + " " + user.lastName }, {
      cssClass: "my-modal"
    });
    modal.present();
  }

  goToDetailUser(user: User) {
    this.navCtrl.push('AdminDetailUserPage', {
      user: user
    });
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
