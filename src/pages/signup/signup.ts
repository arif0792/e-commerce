import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../class/User';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { AccountProvider } from '../../providers/account/account';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  apiUrl: string;
  loginItem: User = new User(null, null, null, null, null, null, null, null, null, [], null);
  password2: string;
  errorMsgs: Map<string, string> = new Map<string, string>();
  checking = false;


  constructor(public navCtrl: NavController, public navParams: NavParams, public accountProvider: AccountProvider,
    public storage: Storage, public alrtCtrl: AlertController, public loadingCtrl: LoadingController) {
    this.storage.get('apiUrl')
      .then(data =>
        this.apiUrl = JSON.parse(data));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  onRegister() {
    let msg = { ID: null, from: "Admin", data: "Selamat datang di aplikasi PANEN, terima kasih telah mendaftar dan menggunakan aplikasi PANEN." }
    this.loginItem.messages.push(msg);
    this.postJSON();
  }

  postJSON() {
    if (this.loginItem.phoneNumber.substring(0, 3) != "+62") {
      let alert = this.alrtCtrl.create({
        title: 'Error',
        message: "Phone number must be Indonesia (+62)",
        buttons: ['Dismiss']
      });
      alert.present();
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Please Wait',
        spinner: 'dots'
      });

      loading.present();

      this.accountProvider.postSaveOrUpdateAccount(this.loginItem)
        .subscribe(
        data => {
          console.log(data);
          this.navCtrl.setRoot('LoginPage');
          loading.dismiss();
        },
        (error: any) => { this.popupError(error); loading.dismiss(); }
        );
    }
  }

  userCheck(ngModel: NgModel) {
    this.checking = true;
    this.accountProvider.userNameCheck(this.loginItem)
      .subscribe(data => {
        console.log(data);
        if (!data.success) {
          this.errorMsgs.set("username", "Username sudah dipakai, silahkan gunakan Username yang lain.");
          ngModel.control.setErrors({ key: 'errorUsername' });
        } else {
          this.errorMsgs.set("username", null);
        }
        this.checking = false;
      },
      (error: any) => { this.popupError(error); }
      );
  }

  emailCheck(ngModel: NgModel) {
    this.checking = true;
    this.accountProvider.emailCheck(this.loginItem)
      .subscribe(data => {
        console.log(data);
        if (!data.success) {
          this.errorMsgs.set("email", "Email sudah dipakai, silahkan gunakan Email yang lain.");
          ngModel.control.setErrors({ key: 'errorEmail' });
        } else {
          this.errorMsgs.set("email", null);
        }
        this.checking = false;
      },
      (error: any) => { this.popupError(error); }
      );
  }

  popupError(obj: any) {
    let alert = this.alrtCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }


}
