import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDetailUserPage } from './admin-detail-user';

@NgModule({
  declarations: [
    AdminDetailUserPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminDetailUserPage),
  ],
})
export class AdminDetailUserPageModule {}
