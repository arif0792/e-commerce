import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../class/User';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { Order } from '../../class/Order';
import { PRODUCT_STATUS } from '../../enum/productStatus';

/**
 * Generated class for the AdminDetailUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-detail-user',
  templateUrl: 'admin-detail-user.html',
})
export class AdminDetailUserPage {
  productStatus = PRODUCT_STATUS;
  user: User;
  orders: Order[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    this.user = this.navParams.get('user');
    this.getOrders();
  }

  getOrders() {
    this.user.orders ? this.user.orders.forEach(element => {
      let obj = new Order(element.ID, element.CreatedAt, element.products, element.totalInstallment, element.totalPrice, element.paymentDetails, element.status, element.orderDate, element.isRefund, element.userId, element.canceledDate, element.completedDate, element.haveCancelDeadline, element.isActive, element.deadlineCancelMonth, element.lebaranMonth);
      this.orders.push(obj);
    }) : null;
  }

  getCurrency(num: number): string {
    return num ? Math.round(num).toLocaleString(undefined, { minimumFractionDigits: 0 }).replace(/,/g, '.') : '0';
  }

  getAngsuran(order: Order) {
    return (order.totalPrice / order.totalInstallment);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminDetailUserPage');
  }

  sendMsg() {
    let modal = this.modalCtrl.create('ModalAdminSendMsgPage', { userId: this.user.ID, userFullName: this.user.firstName + " " + this.user.lastName }, {
      cssClass: "my-modal"
    });
    modal.present();
  }

  goToUserAddresses() {
    this.navCtrl.push('AdminUserAddressesPage', {
      user: this.user
    });
  }

}
