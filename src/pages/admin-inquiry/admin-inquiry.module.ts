import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminInquiryPage } from './admin-inquiry';

@NgModule({
  declarations: [
    AdminInquiryPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminInquiryPage),
  ],
})
export class AdminInquiryPageModule {}
