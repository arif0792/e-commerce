import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { MessageProvider } from '../../providers/message/message';

/**
 * Generated class for the AdminInquiryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-inquiry',
  templateUrl: 'admin-inquiry.html',
})
export class AdminInquiryPage {
  loading: Loading;
  messages: Array<{ from: string, data: string }> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public messageProvider: MessageProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminInquiryPage');
  }

  ionViewWillEnter() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.messageProvider.getInquiries().subscribe(
      data => {
        this.messages = data.data;
        this.loading.dismiss();
      },
      (error: any) => {
        this.popupError(error);
      }
    );

  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  goToDetail(msg: any) {
    this.navCtrl.push('MessageDetailPage', {
      msg: msg
    });
  }

}
