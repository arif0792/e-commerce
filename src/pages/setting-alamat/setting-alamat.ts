import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Address } from '../../class/Address';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Storage } from '@ionic/storage';
import { User } from '../../class/User';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AddressProvider } from '../../providers/address/address';
import { Loading } from 'ionic-angular/components/loading/loading';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

/**
 * Generated class for the SettingAlamatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting-alamat',
  templateUrl: 'setting-alamat.html',
})
export class SettingAlamatPage {
  loading: Loading;
  userData: User;
  addressesTemp: Address[] = [];
  dataArrays: Array<{ address: Address; edit: boolean }> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public alertCtrl: AlertController, public addressProvider: AddressProvider, public loadingCtrl: LoadingController) {
    this.storage.get('userData').then(
      data => {
        let dataObj = JSON.parse(data);
        console.log(dataObj)
        this.userData = dataObj;
        dataObj.addresses ? this.generateAddrArr(this.userData.addresses) : null
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingAlamatPage');
  }

  generateAddrArr(objs: Address[]) {
    this.dataArrays = [];
    this.addressesTemp = [];
    for (let obj of objs) {
      this.dataArrays.push({ address: obj, edit: false });
      this.addressesTemp.push(Object.assign({}, obj));
    }
  }

  setEdit(idx: number) {
    console.log(idx);
    console.log(this.dataArrays);
    this.dataArrays[idx].edit = true;
  }

  addAddress() {
    console.log(this.userData);
    this.dataArrays.push({ address: new Address(null, null, null, null, null, " - ", this.userData.ID), edit: true });
  }

  setCancel(idx: number, ID: number, form: NgForm) {
    this.markAsUntouched(form);
    this.dataArrays[idx].edit = false;

    if (ID) {
      let tempAddress: Address;
      for (let temp of this.addressesTemp) {
        temp.ID === ID ? tempAddress = temp : null;
      }
      for (let data of this.dataArrays) {
        data.address.ID == ID ? data.address = Object.assign({}, tempAddress) : null;
      }
    } else {
      this.dataArrays.splice(idx, 1);
    }
  }

  setDelete(idx: number, form: NgForm) {
    let alert = this.alertCtrl.create({
      title: 'Delete Alamat ?',
      message: 'Apakah anda yakin ingin men-delete alamat ini ?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            console.log('Tidak clicked');
          }
        },
        {
          text: 'Yakin',
          handler: () => {
            this.loading = this.loadingCtrl.create({
              content: 'Please Wait',
              spinner: 'dots'
            });

            this.loading.present();

            this.postJSONDelete(this.dataArrays[idx].address);
            this.dataArrays.splice(idx, 1);
            this.markAsUntouched(form);
          }
        }
      ]
    });
    alert.present();
  }

  onSave(index: number, form: NgForm) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    console.log(this.dataArrays)
    this.dataArrays[index].edit = false;
    this.postJson(this.dataArrays[index].address);
    console.log(form);
    this.markAsUntouched(form); //ijo2 belom ilang
  }

  postJSONDelete(addr: Address) {
    this.addressProvider.deleteAddress(addr)
      .subscribe(data => {
        console.log(data);
        this.setToStorage();
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
      );
  }

  postJson(addr: Address) {
    this.addressProvider.postAddress(addr)
      .subscribe(data => {
        console.log(data);
        this.setToStorage();
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
      );
  }


  markAsUntouched(form: NgForm) {
    Object.keys(form.controls).forEach(control => {
      form.controls[control].markAsUntouched();
    });
  }

  setToStorage() {
    let dataTemp: Address[] = [];
    for(let data of this.dataArrays){
      dataTemp.push(data.address);
    }
    this.userData.addresses = dataTemp;
    this.storage.set('userData', JSON.stringify(this.userData));
    this.loading.dismiss();
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  changeToInt(s: string): number {
    return +s
  }
}
