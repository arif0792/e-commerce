import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SettingAlamatPage } from './setting-alamat';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    SettingAlamatPage
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(SettingAlamatPage),
  ],
})
export class SettingAlamatPageModule {}
