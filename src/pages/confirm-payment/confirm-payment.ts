import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ConfirmPaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-payment',
  templateUrl: 'confirm-payment.html',
})
export class ConfirmPaymentPage {
  resultCode : string = '';
  vaNumber: string = '';
  totalPembayaran: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {
    let mapData = this.navParams.get('mapData');
    this.resultCode = mapData.get('resultCode');
    this.vaNumber = mapData.get('vaNumber');
    this.totalPembayaran = parseInt(mapData.get('totalPembayaran'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmPaymentPage');
  }

  onClickAlamat() {
    this.navCtrl.setRoot('AccountSettingPage', { name: 'alamat' });
  }

  onClickHome() {
    this.navCtrl.setRoot('HomePage');
  }

  getCurrency(num: number): string {
    return num.toLocaleString(undefined, { minimumFractionDigits: 0 }).replace(/,/g, '.');
  }
}
