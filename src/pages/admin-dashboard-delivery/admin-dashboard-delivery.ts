import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProductProvider } from '../../providers/product/product';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { PRODUCT_STATUS } from '../../enum/productStatus';
import { AccountProvider } from '../../providers/account/account';
import { User } from '../../class/User';
import { Product } from '../../class/Product';
import { MessageProvider } from '../../providers/message/message';

/**
 * Generated class for the AdminDashboardDeliveryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-dashboard-delivery',
  templateUrl: 'admin-dashboard-delivery.html',
})
export class AdminDashboardDeliveryPage {
  statusProduct = PRODUCT_STATUS;
  mapProduct: Map<string, Array<any>> = new Map<string, Array<any>>();
  mapKeys: Array<string> = [];
  productcount: number = 0;
  currentProductCount: number = 0;
  currentActiveProductsCount: number;
  currentOnDeliveryProductsCount: number;
  loading: Loading;
  querysearch: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public productProvider: ProductProvider, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public modalCtrl: ModalController, public accountProvider: AccountProvider, public messageProvider: MessageProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminDashboardDeliveryPage');
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.querysearch = "";
    this.getProductsByIds(this.querysearch, 0);
  }

  getProductsByIds(search: string, start: number, infScroll?: any) {
    this.productProvider.getProductOrderByKelurahanCount(search).subscribe(
      data => {
        this.productcount = data.data;
        this.productProvider.getProductOrderByKelurahan(search, start).subscribe(
          data => {
            if (start != 0) {
              this.currentProductCount += data.data.length;
            } else {
              this.mapProduct = new Map<string, Array<any>>();
              this.currentProductCount = data.data.length;
            }
            this.currentActiveProductsCount = 0;
            this.currentOnDeliveryProductsCount = 0;
            for (let prod of data.data) {
              if (this.mapProduct.get(prod.kelurahan)) {
                this.mapProduct.get(prod.kelurahan).push(prod);
              } else {
                this.mapProduct.set(prod.kelurahan, []);
                this.mapProduct.get(prod.kelurahan).push(prod);
              }
              if (prod.status === 0) {
                this.currentActiveProductsCount += 1;
              } else if (prod.status === 1) {
                this.currentOnDeliveryProductsCount += 1;
              }
            }
            this.mapKeys = Array.from(this.mapProduct.keys());
            this.loading.dismiss();
            infScroll ? infScroll.complete() : null;
          },
          (error: any) => { this.popupError(error); this.loading.dismiss(); }
        );
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
  }

  sendMessage(prod: any) {
    let modal = this.modalCtrl.create('ModalAdminSendMsgPage', { userId: prod.userId, userFullName: prod.firstName + " " + prod.lastName }, {
      cssClass: "my-modal"
    });
    modal.present();
  }

  goToUserDetail(prod: any) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    let user: User;
    this.accountProvider.getUserByID(prod.userId).subscribe(
      data => {
        user = data.data;
        this.navCtrl.push('AdminDetailUserPage', {
          user: user
        });
        this.loading.dismiss();
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );

  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  onEnter(value: string) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.querysearch = value;
    this.getProductsByIds(this.querysearch, 0);
  }

  doLoadMoreData(infiniteScroll) {
    this.getProductsByIds(this.querysearch, this.currentProductCount, infiniteScroll);
  }

  saveProdStatus(prod: any, status: number) {
    let alert = this.alertCtrl.create({
      title: status == 1 ? 'Kirim' : 'Done' + ' Order',
      message: status == 1 ? 'Apakah anda yakin ingin kirim order ini?' : 'Apakah anda yakin ingin done order ini?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            console.log('Tidak clicked');
          }
        },
        {
          text: 'Yakin',
          handler: () => {
            console.log('Yakin clicked');
            this.loading = this.loadingCtrl.create({
              content: 'Please Wait',
              spinner: 'dots'
            });

            this.loading.present();

            prod.status = status;
            let prodArrayForSave: Product[] = [];
            prodArrayForSave.push(new Product(prod.ID, prod.CreatedAt, prod.productId, prod.status, prod.alamatId, prod.orderId, prod.itemName, prod.itemPrice, prod.itemAdminPrice, prod.itemOptionPrice, prod.itemCategory, prod.itemOption));
            this.productProvider.saveProducts(prodArrayForSave).subscribe(
              data => {
                let msg: any;
                if (status == 1) {
                  msg = { from: "Admin", data: "Order #" + prod.orderId.toString() + " dengan no kambing #" + prod.productId + " sedang dikirim.", userId: prod.userId };
                } else {
                  msg = { from: "Admin", data: "Order #" + prod.orderId.toString() + " dengan no kambing #" + prod.productId + " sudah terkirim.", userId: prod.userId };
                }
                this.postMessage(msg).then(
                  () => {
                    console.log(data);
                    this.getProductsByIds(this.querysearch, this.currentProductCount - 5 < 0 ? 0 : this.currentProductCount - 5);
                  }
                );
              },
              (error: any) => { this.popupError(error); this.loading.dismiss(); }
            );
          }
        }
      ]
    });
    alert.present();
  }

  postMessage(msg: any) {
    return new Promise((resolve, reject) => {
      this.messageProvider.postMessage(msg).subscribe(
        data => {
          console.log(data);
          resolve();
        },
        (error: any) => {
          this.popupError(error);
          this.loading.dismiss();
        }
      );
    });
  }

}
