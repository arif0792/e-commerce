import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDashboardDeliveryPage } from './admin-dashboard-delivery';

@NgModule({
  declarations: [
    AdminDashboardDeliveryPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminDashboardDeliveryPage),
  ],
})
export class AdminDashboardDeliveryPageModule {}
