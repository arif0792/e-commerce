import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Order } from '../../class/Order';
import { Storage } from '@ionic/storage';
import { User } from '../../class/User';

/**
 * Generated class for the AccountSettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account-setting',
  templateUrl: 'account-setting.html',
})
export class AccountSettingPage {
  orders: Order[] = [];
  errorMsg: string[];
  userData: User;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) {

  }

  ionViewWillEnter() {
    this.orders = [];
    this.storage.get('userData').then(
      data => {
        this.userData = JSON.parse(data);
        this.userData.orders ? this.userData.orders.forEach(element => {
          let obj = new Order(element.ID, element.CreatedAt, element.products, element.totalInstallment, element.totalPrice, element.paymentDetails, element.status, element.orderDate, element.isRefund, element.userId, element.canceledDate, element.completedDate, element.haveCancelDeadline, element.isActive, element.deadlineCancelMonth, element.lebaranMonth);
          this.orders.push(obj);
        }) : null;
      },
      error => this.errorMsg
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountSettingPage');
  }

  forwardToAlamat() {
    this.navCtrl.push('SettingAlamatPage');
  }

  forwardToOrderAlamat(obj: Order) {
    this.navCtrl.push('DetailSettingAlamatPengirimanPage', {
      order: obj
    });
  }

  forwardToEditProfile() {
    this.navCtrl.push('EditprofilePage', {
      userData: this.userData
    });
  }

  forwardToChangePass() {
    this.navCtrl.push('ChangepasswordPage', {
      userData: this.userData
    });
  }

}
