import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditprofilePage } from './editprofile';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    EditprofilePage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(EditprofilePage),
  ],
})
export class EditprofilePageModule {}
