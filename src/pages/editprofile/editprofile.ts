import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AccountProvider } from '../../providers/account/account';
import { User } from '../../class/User';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { AngularFireAuth } from 'angularfire2/auth';
import { Storage } from '@ionic/storage';
import { User as UserFirebase } from '@firebase/auth-types';

/**
 * Generated class for the EditprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editprofile',
  templateUrl: 'editprofile.html',
})
export class EditprofilePage {
  loading: Loading;
  userData: User;
  userDataTemp: User;
  userFirebase: UserFirebase;

  constructor(public navCtrl: NavController, public navParams: NavParams, public accountProvider: AccountProvider,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController, public firebase: AngularFireAuth,
    public storage: Storage) {

    this.userData = this.navParams.get('userData');
    this.userDataTemp = Object.assign({}, this.userData);
    this.firebase.authState.subscribe(user => { this.userFirebase = user });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofilePage');
  }

  onSubmit() {
    if (this.userData.phoneNumber.substring(0, 3) != "+62") {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: "Phone number must be Indonesia (+62)",
        buttons: ['Dismiss']
      });
      alert.present();
    } else {
      console.log(this.userData);
      this.loading = this.loadingCtrl.create({
        content: 'Please Wait',
        spinner: 'dots'
      });

      this.loading.present();
      this.save();
    }
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  save() {
    this.accountProvider.postSaveOrUpdateAccount(this.userData).subscribe(
      data => {
        this.firebase.auth.signOut().then(
          () => {
            this.firebase.auth.signInWithEmailAndPassword(data.data.email, data.data.password).then(
              () => {
                this.storage.set('userData', JSON.stringify(<User>data.data));
                let alert = this.alertCtrl.create({
                  title: 'Berhasil',
                  message: "Profile anda berhasil di rubah",
                  buttons: ['OK']
                });
                this.userDataTemp = Object.assign({}, this.userData);
                this.loading.dismiss();
                alert.present();
              },
              (error: any) => { this.popupError(error); this.loading.dismiss(); }
            );
          },
          (error: any) => { this.popupError(error); this.loading.dismiss(); }
        );
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
  }

}


