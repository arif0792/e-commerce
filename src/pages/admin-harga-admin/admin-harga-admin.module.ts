import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminHargaAdminPage } from './admin-harga-admin';

@NgModule({
  declarations: [
    AdminHargaAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminHargaAdminPage),
  ],
})
export class AdminHargaAdminPageModule {}
