import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';
import { ItemDetail } from '../../class/ItemDetail';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-admin-harga-admin',
  templateUrl: 'admin-harga-admin.html',
})
export class AdminHargaAdminPage {
  itemDetails: ItemDetail[] = [];
  mapItems: Map<string, number> = new Map<string, number>();
  mapKeys;
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public itemProvider: ItemProvider, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminHargaAdminPage');
  }

  ionViewWillEnter() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.itemProvider.getAllItems().subscribe(
      data => {
        this.itemDetails = data;
        for (let obj of data) {
          this.mapItems.set(obj.itemCategory, obj.itemAdminPrice);
        }
        this.mapKeys = Array.from(this.mapItems.keys());
        this.loading.dismiss();
      }
    );
  }

  onChange(key: string, event: any) {
    this.mapItems.set(key, parseInt(event.value));
  }

  onSubmit() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    for (let item of this.itemDetails) {
      for (let key of this.mapKeys) {
        if (item.itemCategory == key) {
          item.itemAdminPrice = this.mapItems.get(key);
        }
      }
    }
    this.itemProvider.saveItems(this.itemDetails).subscribe(
      data => {
        console.log(data);
        this.loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Berhasil',
          message: 'Berhasil',
          buttons: ['Ok']
        });
        alert.present();
      },
      (error: any) => {
        this.popupError(error);
        this.loading.dismiss();
      }
    );
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
