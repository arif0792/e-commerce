import { Component } from '@angular/core';
import { NavController, IonicPage, NavParams, AlertController } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';
import { SettingsProvider } from '../../providers/settings/settings';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  itemImages: ByteString[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
    public itemProvider: ItemProvider, public settingProvider: SettingsProvider) {
    this.settingProvider.getSettingBannerPicIds().subscribe(
      data => {
        let ids = JSON.parse(data);
        this.itemProvider.getItemPictures(ids.id).subscribe(
          data => {
            console.log(data);
            this.itemImages = [];
            for (let pic of data.data) {
              this.itemImages.push("data:image/jpeg;base64," + pic.data);
            }
            console.log(this.itemImages);
          },
          (error: any) => { this.popupError(error); }
        );
      },
      (error: any) => { this.popupError(error); }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  openPage(page: string) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.navCtrl.setRoot(page);
  }

}
