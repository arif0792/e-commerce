import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDashboardRefundPage } from './admin-dashboard-refund';

@NgModule({
  declarations: [
    AdminDashboardRefundPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminDashboardRefundPage),
  ],
})
export class AdminDashboardRefundPageModule {}
