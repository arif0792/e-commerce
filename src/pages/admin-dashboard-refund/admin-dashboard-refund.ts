import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular'; 0
import { AccountProvider } from '../../providers/account/account';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Order } from '../../class/Order';
import { OrderProvider } from '../../providers/order/order';
import { MessageProvider } from '../../providers/message/message';

/**
 * Generated class for the AdminDashboardRefundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-dashboard-refund',
  templateUrl: 'admin-dashboard-refund.html',
})
export class AdminDashboardRefundPage {

  ordersArray: Array<{ order: Order, firstName: string, lastName: string }> = [];
  ordercount: number;
  loading: Loading;
  querysearch: string;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public accountProvider: AccountProvider, public loadingCtrl: LoadingController, public messageProvider: MessageProvider,
    public modalCtrl: ModalController, public alertCtrl: AlertController, public orderProvider: OrderProvider) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.querysearch = "";
    this.getOrderById(this.querysearch, 0);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminDashboardUserPage');
  }

  onEnter(value: string) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.querysearch = value;
    this.getOrderById(this.querysearch, 0);
  }

  doLoadMoreData(infiniteScroll) {
    this.getOrderById(this.querysearch, this.ordersArray.length, infiniteScroll);
  }

  getOrderById(name: string, start: number, infScroll?: any) {
    this.orderProvider.getOrdersRefundByIDCount(name).subscribe(
      data => {
        this.ordercount = data.data;
        this.orderProvider.getOrdersRefundByID(name, start).subscribe(
          data => {
            if (start == 0) {
              this.ordersArray = [];
            }
            for (let ord of data.data) {
              this.ordersArray.push({ order: new Order(ord.ID, ord.CreatedAt, ord.products, ord.totalInstallment, ord.totalPrice, ord.paymentDetails, ord.status, ord.orderDate, ord.isRefund, ord.userId, ord.canceledDate, ord.completedDate, ord.haveCancelDeadline, ord.isActive, ord.deadlineCancelMonth, ord.lebaranMonth), firstName: ord.firstName, lastName: ord.lastName });
            }
            console.log(this.ordersArray);
            this.loading.dismiss();
            infScroll ? infScroll.complete() : null;
          },
          (error: any) => { this.popupError(error); this.loading.dismiss(); }
        );
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
  }

  sendMessage(order: any) {
    let modal = this.modalCtrl.create('ModalAdminSendMsgPage', { userId: order.order.userId, userFullName: order.firstName + " " + order.lastName }, {
      cssClass: "my-modal"
    });
    modal.present();
  }

  goToDetailUser(id: number) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.accountProvider.getUserByID(id).subscribe(
      data => {
        this.navCtrl.push('AdminDetailUserPage', {
          user: data.data
        });
        this.loading.dismiss();
      }
    );
  }

  saveOrderRefundDone(order: Order) {
    let alert = this.alertCtrl.create({
      title: 'Refund',
      message: 'Apakah anda yakin ingin menyelesaikan refund order ini?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            console.log('Tidak clicked');
          }
        },
        {
          text: 'Yakin',
          handler: () => {
            console.log('Yakin clicked');
            this.loading = this.loadingCtrl.create({
              content: 'Please Wait',
              spinner: 'dots'
            });

            this.loading.present();

            order.isRefund = false;
            this.orderProvider.postOrder(order).subscribe(
              data => {
                let msg = { from: "Admin", data: "Order #" + order.ID.toString() + " telah selesai di refund.", userId: order.userId };
                this.postMessage(msg).then(
                  () => {
                    console.log(data);
                    this.getOrderById(this.querysearch, this.ordersArray.length - 5 < 0 ? 0 : this.ordersArray.length - 5);
                  }
                );
              }
            );
          }
        }
      ]
    });
    alert.present();
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  getCurrency(num: number): string {
    return num.toLocaleString(undefined, { minimumFractionDigits: 0 }).replace(/,/g, '.');
  }

  postMessage(msg: any) {
    return new Promise((resolve, reject) => {
      this.messageProvider.postMessage(msg).subscribe(
        data => {
          console.log(data);
          resolve();
        },
        (error: any) => {
          this.popupError(error);
          this.loading.dismiss();
        }
      );
    });
  }

}
