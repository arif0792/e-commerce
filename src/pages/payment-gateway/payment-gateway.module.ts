import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentGatewayPage } from './payment-gateway';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [
    PaymentGatewayPage,
  ],
  imports: [
    DirectivesModule,
    IonicPageModule.forChild(PaymentGatewayPage),
  ],
})
export class PaymentGatewayPageModule {}
