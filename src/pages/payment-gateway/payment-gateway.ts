import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Order } from '../../class/Order';
import { PaymentDetail } from '../../class/PaymentDetail';
import { CountryProvider } from '../../providers/country/country';
import { Storage } from '@ionic/storage';
import { User } from '../../class/User';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { OrderProvider } from '../../providers/order/order';
import { MessageProvider } from '../../providers/message/message';
import { Events } from 'ionic-angular/util/events';
import { Loading } from 'ionic-angular/components/loading/loading';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { DuitkuProvider } from '../../providers/duitku/duitku';
import { RequestDuitkuInquiry } from '../../class/duitku/RequestDuitkuInquiry';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { ResponseDuitkuInquiry } from '../../class/duitku/ResponseDuitkuInquiry';
import { PaymentDuitkuDetail } from '../../class/duitku/PaymentDuitkuDetail';
import { Status } from '../../enum/status';
/**
 * Generated class for the PaymentGatewayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payment-gateway',
  templateUrl: 'payment-gateway.html',
})
export class PaymentGatewayPage {
  paymentDetailsSum = 0;
  price = 0;
  temp = false;
  mapData = new Map<string, string>();
  responseDuitkuInquiry: ResponseDuitkuInquiry;
  paymentUrl: string;
  apiUrl: string;
  userData: User;
  order: Order;
  orderTemp: Order;
  paymentDetails: PaymentDetail[] = [];
  paymentMethods: Array<{ name: string, value: string }>;
  cards: Array<string> = [];
  // countries: Array<{ name: string, code: string }> = [];
  totalInstallmentNumberArray: Array<{ number: number, checked: boolean }> = [];
  selectedPayMethod: string;
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public countryProvider: CountryProvider,
    public storage: Storage, public orderProvider: OrderProvider, public alertCtrl: AlertController, public modal: ModalController,
    public messageProvider: MessageProvider, public event: Events, public loadingCtrl: LoadingController, public duitkuProvider: DuitkuProvider) {
    this.storage.get('apiUrl')
      .then(data =>
        this.apiUrl = JSON.parse(data));
    this.storage.get('userData').then(
      data => this.userData = JSON.parse(data)
    );
    this.order = this.navParams.get('order');
    this.orderTemp = new Order(this.order.ID, this.order.CreatedAt, this.order.products, this.order.totalInstallment,
      this.order.totalPrice, this.order.paymentDetails, this.order.status, this.order.orderDate, this.order.isRefund, this.order.userId, this.order.canceledDate, this.order.completedDate, this.order.haveCancelDeadline, this.order.isActive, this.order.deadlineCancelMonth, this.order.lebaranMonth);
    // this.countryProvider.getCountries().subscribe(
    //   data => this.countries = data,
    //   (error: any) => {this.popupError(error); this.loading.dismiss(); }
    // );
    this.paymentMethods = [
      { name: 'Credit Card', value: 'creditcard' },
      { name: 'Transfer Bank', value: 'banktransfer' }
    ];
    this.cards.push('VISA'); this.cards.push('MASTER CARD');

    this.order.paymentDetails ? this.paymentDetailsSum = this.order.paymentDetails.length : this.paymentDetailsSum = 0;
    if (this.paymentDetailsSum < this.order.totalInstallment) {
      for (let i = this.order.getCurrentInstallment() + 1; i <= this.order.totalInstallment; i++) {
        this.totalInstallmentNumberArray.push({
          number: i,
          checked: i == this.order.getCurrentInstallment() + 1 ? true : false
        });
      }
    } else if (this.paymentDetailsSum == this.order.totalInstallment) {
      for (let prd of this.order.products) {
        this.price += prd.itemOptionPrice;
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentGatewayPage');
    console.log(this.order);
  }

  onSubmit() {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    if (!this.order.ID) {
      this.temp = true;
      this.order.isActive = false;
      this.orderProvider.postOrder(this.order).subscribe(
        data => {
          this.order = new Order(data.data.ID, data.data.CreatedAt, data.data.products, data.data.totalInstallment, data.data.totalPrice, data.data.paymentDetails, data.data.status, data.data.orderDate, data.data.isRefund, data.data.userId,
            data.data.canceledDate, data.data.completedDate, data.data.haveCancelDeadline, data.data.isActive, data.data.deadlineCancelMonth, data.data.lebaranMonth);
          console.log(this.order);
          this.doSave();
        },
        (error: any) => {
          this.popupError(error);
          this.showAlertError();
          this.loading.dismiss();
        }
      );
    } else {
      this.doSave();
    }

  }

  getOrderIdForDuitku(): string {
    let str = "#" + this.order.ID.toString();
    let current = this.order.getCurrentInstallment();
    for (let i = current + 1; i <= current + this.getCount(); i++) {
      str += "|" + i;
    }
    console.log(str);
    return str;
  }

  doSave() {
    let paymentMethod: string = this.selectedPayMethod == 'creditcard' ? "VC" : this.selectedPayMethod == 'banktransfer' ? "A1" : null;
    let request: RequestDuitkuInquiry;
    if (this.paymentDetailsSum < this.order.totalInstallment) {
      request = new RequestDuitkuInquiry(Math.round(this.getTotalPembayaran()), this.getOrderIdForDuitku(), this.order.getProdDesc()[0], this.userData.email, paymentMethod, "");
    } else {
      request = new RequestDuitkuInquiry(this.price, this.getOrderIdForDuitku(), this.order.getProdDesc()[0], this.userData.email, paymentMethod, "");
    }
    console.log(request);
    this.duitkuProvider.postInquiry(request).subscribe(
      data => {
        console.log(data);
        this.responseDuitkuInquiry = data;
        if (this.responseDuitkuInquiry.paymentUrl) {
          this.paymentUrl = data.paymentUrl;
          let modal = this.modal.create('ModalPaymentgatewayPage', { url: this.paymentUrl }, {
            cssClass: "my-modal"
          });
          modal.onDidDismiss((data: Map<string, string>) => {
            this.mapData = data;
            this.mapData.set('vaNumber', this.responseDuitkuInquiry.vaNumber);
            if (this.paymentDetailsSum < this.order.totalInstallment) {
              this.mapData.set('totalPembayaran', Math.round(this.getTotalPembayaran()).toString());
            } else {
              this.mapData.set('totalPembayaran', this.price.toString());
            }
            console.log(this.mapData);
            if ( (this.selectedPayMethod == 'banktransfer'
              && this.mapData.get('resultCode') == '01')
              || (this.selectedPayMethod == 'creditcard'
              && this.mapData.get('resultCode') == '00') ) {

              this.saveToDB(this.mapData);

            } else if (this.mapData.get('resultCode') == '01'
              && this.temp) {
              this.showAlertError();
            } else if (this.mapData.get('resultCode') == '02') {
              //nothing canceled cc(back)
            }
          });
          this.loading.dismiss();
          modal.present();
          //DEV
          // this.mapData.set('resultCode', '00');
          // this.mapData.set('reference', '12342342341');
          // this.mapData.set('vaNumber', this.responseDuitkuInquiry.vaNumber);
          // this.mapData.set('totalPembayaran', this.order.totalPrice.toString());
          // this.saveToDB(this.mapData);
        } else {
          this.popupError({ message: "URL PAYMENT GATEWAY ERROR!" });
          this.showAlertError();
          this.loading.dismiss();
        }
      },
      (error: any) => {
        this.popupError(error);
        this.showAlertError();
        this.loading.dismiss();
      }
    );
  }

  saveToDB(mapData: Map<string, string>) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.order.isActive = true;
    this.order.isRefund = false;

    if (!this.order.paymentDetails) {
      this.order.paymentDetails = [];
    }

    if (this.paymentDetailsSum < this.order.totalInstallment) {
      let current = this.order.getCurrentInstallment();
      for (let i = current + 1; i <= current + this.getCount(); i++) {
        let paymentDetail = new PaymentDetail(null, this.selectedPayMethod, i, this.order.ID, new Date(), null);
        // else if (paymentDetail.payMethod == 'banktransfer') {
        //   this.paymentBankTransfer.nominal = this.order.getCicilan();
        //   this.paymentBankTransfer.rekeningNo = '1234-1234-1234-1234';
        //   this.paymentBankTransfer.paymentDate = new Date();
        //   paymentDetail.paymentBankTransfer = this.paymentBankTransfer;
        //   paymentDetail.paymentCreditCard = null;
        // }


        //
        let paymentDuitkuDetail = new PaymentDuitkuDetail(null, this.responseDuitkuInquiry.reference, this.mapData.get('resultCode'), this.selectedPayMethod, parseInt(this.mapData.get('totalPembayaran')) / this.getCount(), this.order.getCurrentInstallment() + 1, this.order.ID, paymentDetail.ID, this.mapData.get('vaNumber'));
        paymentDetail.paymentDuitkuDetail = paymentDuitkuDetail;
        //
        this.order.paymentDetails.push(paymentDetail);
        console.log(paymentDuitkuDetail);

      }
    } else if (this.paymentDetailsSum == this.order.totalInstallment) {
      let paymentDetail = new PaymentDetail(null, this.selectedPayMethod, this.order.totalInstallment + 1, this.order.ID, new Date(), null);
      let paymentDuitkuDetail = new PaymentDuitkuDetail(null, this.responseDuitkuInquiry.reference, this.mapData.get('resultCode'), this.selectedPayMethod, this.price, this.order.totalInstallment + 1, this.order.ID, paymentDetail.ID, this.mapData.get('vaNumber'));
      paymentDetail.paymentDuitkuDetail = paymentDuitkuDetail;
      this.order.paymentDetails.push(paymentDetail);
      console.log(paymentDuitkuDetail);
    }

    //status ganti ke 1 active / waiting for payment
    if (mapData.get('resultCode') == '01') {
      this.order.status = Status.WAITING_FOR_PAYMENT;
    } else if (mapData.get('resultCode') == '00') {
      this.order.status = Status.ACTIVE;
    }

    console.log(this.order);
    this.orderProvider.postOrder(this.order)
      .subscribe(
        data => {
          let ord: Order = data.data;
          if (!data.success) {
            let alert = this.alertCtrl.create({
              title: 'Terjadi Kesalahan',
              message: 'Mohon maaf transaksi error, silahkan dicoba kembali',
              buttons: [
                {
                  text: 'OK',
                  handler: () => {
                    console.log(data);
                  }
                }
              ]
            });
            alert.present();
          } else {
            this.order = new Order(ord.ID, ord.CreatedAt, ord.products, ord.totalInstallment, ord.totalPrice, ord.paymentDetails, ord.status, ord.orderDate, ord.isRefund, ord.userId, ord.canceledDate, ord.completedDate, ord.haveCancelDeadline, ord.isActive, ord.deadlineCancelMonth, ord.lebaranMonth);
            this.initRefreshOrdersData();
          }
        },
        (error: any) => {
          this.popupError(error);
          this.showAlertError();
          this.loading.dismiss();
          this.order = this.orderTemp;
        });
  }

  initRefreshOrdersData() {
    this.order.paymentDetails ? this.paymentDetailsSum = this.order.paymentDetails.length : this.paymentDetailsSum = 0;
    //
    let msg;
    let postMsg = false;
    let msg2;
    let postMsg2 = false;
    let deliveryMsg = false;
    for (let prd of this.order.products) {
      if (prd.itemOption == 'ANTAR' && prd.alamatId == 0) {
        deliveryMsg = true;
      }
    }
    if (this.paymentDetailsSum == this.order.totalInstallment) {
      msg = { ID: null, userId: this.userData.ID, from: "Admin", data: "Mohon untuk membayar pembayaran antar/potong untuk order: " + this.order.ID + ". Silahkan untuk membayar di Account Setting atau Dashboard. Terima kasih." }
      postMsg = true;
    }
    if (deliveryMsg) {
      msg2 = { ID: null, userId: this.userData.ID, from: "Admin", data: "Mohon untuk men-setting alamat untuk order: " + this.order.ID + ". Silahkan untuk mem-setting alamat di Account Setting atau Dashboard. Terima kasih." }
      postMsg2 = true;
    }
    //
    if (postMsg) {
      this.postMessage(msg).then(
        () => {
          if (postMsg2) {
            this.postMessage(msg2).then(
              () => {
                this.refreshOrders();
              });
          } else {
            this.refreshOrders();
          }
        }
      );
    } else if (postMsg2) {
      this.postMessage(msg2).then(
        () => {
          this.refreshOrders();
        });
    } else {
      this.refreshOrders();
    }
  }

  refreshOrders() {
    this.userData.orders.splice(0,0, this.order);
    this.storage.set('userData', JSON.stringify(this.userData)).then(
      () => {
        this.navCtrl.setRoot('ConfirmPaymentPage', {
          mapData: this.mapData
        });
        this.loading.dismiss();
      }
    );
  }

  getCurrency(num: number): string {
    return Math.round(num).toLocaleString(undefined, { minimumFractionDigits: 0 }).replace(/,/g, '.');
  }

  getTotalPembayaran() {
    return this.getAngsuran() * this.getCount();
  }

  getCount(): number {
    let count = 0;
    for (let obj of this.totalInstallmentNumberArray) {
      if (obj.checked) {
        count += 1;
      }
    }
    return count;
  }

  getAngsuran() {
    return (this.order.totalPrice / this.order.totalInstallment);
  }

  postMessage(msg: any) {
    return new Promise((resolve, reject) => {
      this.messageProvider.postMessage(msg).subscribe(
        data => {
          console.log(data);
          resolve();
        },
        (error: any) => {
          this.popupError(error);
          this.showAlertError();
          this.loading.dismiss();
        }
      );
    });
  }


  // getMessages() {
  //   return new Promise((resolve, reject) => {
  //     this.messageProvider.getMessages(this.userData.ID.toString()).subscribe(
  //       data => {
  //         this.userData.messages = data.data;
  //         this.messageProvider.getReads(this.userData.ID.toString()).subscribe(
  //           data => {
  //             this.userData.reads = data.data;
  //             let badgeCount = 0, readCount = 0;
  //             for (let msg of this.userData.messages) {
  //               for (let msgRead of data.data) {
  //                 if (msgRead.messageId == msg.ID
  //                   && msgRead.read) {
  //                   readCount += 1;
  //                   break;
  //                 }
  //               }
  //             }
  //             badgeCount = this.userData.messages.length - readCount;
  //             this.event.publish('messagesBadge', badgeCount);
  //             this.storage.set('messagesBadge', badgeCount);
  //             resolve();
  //           }
  //         )
  //       },
  //       (error: any) => {
  //         this.popupError(error);
  //         this.showAlertError();
  //         this.loading.dismiss();
  //       }
  //     );
  //   });
  // }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  showAlertError() {
    let alert = this.alertCtrl.create({
      title: 'Terjadi Kesalahan',
      message: 'Mohon maaf transaksi error, silahkan dicoba kembali',
      buttons: [
        {
          text: 'OK',
          handler: () => {

            console.log(this.mapData);
          }
        }
      ]
    });
    alert.present();
  }


}

