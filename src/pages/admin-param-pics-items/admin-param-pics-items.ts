import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { ItemProvider } from '../../providers/item/item';
import { CameraOptions, Camera } from '@ionic-native/camera';
import { ItemDetail } from '../../class/ItemDetail';

/**
 * Generated class for the AdminParamPicsItemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-param-pics-items',
  templateUrl: 'admin-param-pics-items.html',
})
export class AdminParamPicsItemsPage {
  loading: Loading
  items: Array<any> = [];
  images: Array<{ ID: number, data: ByteString }> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public itemProvider: ItemProvider, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public camera: Camera) {

    this.items = this.navParams.get('items');
    if (this.items.length > 0
      && this.items[0].itemPictureIds) {
      let ids = JSON.parse(this.items[0].itemPictureIds);
      this.itemProvider.getItemPictures(ids.id).subscribe(
        data => {
          console.log(data);
          this.images = [];
          for (let pic of data.data) {
            this.images.push({ ID: pic.ID, data: "data:image/jpeg;base64," + pic.data });
          }
          console.log(this.images);
        },
        (error: any) => { this.popupError(error); }
      );
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminParamPicsItemsPage');
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  setDelete(idx: number) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.itemProvider.deleteItemPicture(this.images[idx].ID.toString()).subscribe(
      () => {
        this.images.splice(idx, 1);
        this.loading.dismiss();
      },
      (error: any) => { this.popupError(error); this.loading.dismiss(); }
    );
  }

  addPic() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then(
      (imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.loading = this.loadingCtrl.create({
          content: 'Please Wait',
          spinner: 'dots'
        });

        this.loading.present();

        let forSave: Array<{ ID: number, data: ByteString }> = [];
        forSave.push({ ID: 0, data: imageData });
        this.itemProvider.postItemPictures(forSave).subscribe(
          data => {
            console.log(data.data);
            data.data[0].data = "data:image/jpeg;base64," + data.data[0].data;
            this.images.push(data.data[0]);
            let tempArray: { id: string[] };
            tempArray = { id: [] };
            for (let item of this.images) {
              tempArray.id.push(item.ID.toString());
            }

            let itemDetails: ItemDetail[] = [];
            for (let obj of this.items) {
              obj.itemPictureIds = JSON.stringify(tempArray);
              itemDetails.push(obj);
            }

            this.itemProvider.saveItems(itemDetails).subscribe(
              data => {
                console.log();
                this.loading.dismiss();
                let alert = this.alertCtrl.create({
                  title: 'Berhasil',
                  message: 'Berhasil',
                  buttons: ['Ok']
                });
                alert.present();
              },
              (error: any) => {
                this.popupError(error);
                this.loading.dismiss();
              }
            );

          },
          (error: any) => { this.popupError(error); this.loading.dismiss(); }
        );

      }, (error: any) => {
        {
          if (error.message) {
            this.popupError(error);
          }
        }
      });
  }
}
