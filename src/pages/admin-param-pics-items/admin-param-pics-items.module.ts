import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminParamPicsItemsPage } from './admin-param-pics-items';

@NgModule({
  declarations: [
    AdminParamPicsItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminParamPicsItemsPage),
  ],
})
export class AdminParamPicsItemsPageModule {}
