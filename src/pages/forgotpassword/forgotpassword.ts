import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

/**
 * Generated class for the ForgotpasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {
  email: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public firebase: AngularFireAuth,
    public loadingCtrl: LoadingController, public alrtCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpasswordPage');
  }

  resetPassword() {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    loading.present();
    this.firebase.auth.sendPasswordResetEmail(this.email).then(
      () => {
        loading.dismiss();
        let alert = this.alrtCtrl.create({
          title: 'Reset Password',
          message: "Reset Password telah dikiriman ke email anda",
          buttons: ['OK']
        });
        alert.present();
      },
      (error: any) => {this.popupError(error); loading.dismiss(); }
    );
  }

  popupError(obj: any) {
    let alert = this.alrtCtrl.create({
      title: 'Reset Password',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
