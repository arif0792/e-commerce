import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDashboardHistoryPage } from './admin-dashboard-history';

@NgModule({
  declarations: [
    AdminDashboardHistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminDashboardHistoryPage),
  ],
})
export class AdminDashboardHistoryPageModule {}
