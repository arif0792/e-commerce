import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular/util/events';
import { User } from '../../class/User';
import { AccountProvider } from '../../providers/account/account';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Platform } from 'ionic-angular/platform/platform';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { AngularFireAuth } from 'angularfire2/auth';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  apiUrl: string;
  loginItem: User = new User(null, null, null, null, null, null, null, null, null, null, null);
  errorMsg: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public event: Events, public accountProvider: AccountProvider, public alrtCtrl: AlertController, public platform: Platform,
    public loadingCtrl: LoadingController, public firebase: AngularFireAuth) {

    this.storage.get('apiUrl')
      .then(data =>
        this.apiUrl = JSON.parse(data));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  gotoSignUp() {
    this.navCtrl.setRoot('SignupPage');
  }

  gotoForgotPassword() {
    this.navCtrl.push('ForgotpasswordPage');
  }

  onLogin() {
    let loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    loading.present();

    this.firebase.auth.signInWithEmailAndPassword(this.loginItem.email, this.loginItem.password).then(
      data => {
        console.log(data);
        this.accountProvider.login(this.loginItem).subscribe(
          data => {
            this.storage.set('userData', JSON.stringify(<User>data.data)).then(
              () => {
                this.event.publish('login', true);
                let badgeCount = 0, readCount = 0;
                for (let msg of data.data.messages) {
                  if (data.data.reads) {
                    for (let msgRead of data.data.reads) {
                      if (msgRead.messageId == msg.ID
                        && msgRead.read) {
                        readCount += 1;
                        break;
                      }
                    }
                  }
                }
                badgeCount = data.data.messages.length - readCount;
                this.event.publish('messagesBadge', badgeCount);
                this.storage.set('messagesBadge', badgeCount);
                let temp, isAdmin;
                if (!data.data.adminPrivileges) {
                  temp = { dashboardDelivery: false, dashboardOrder: false, dashboardOverdue: false, dashboardRefund: false, settings: false };
                  isAdmin = false;
                } else {
                  isAdmin = true;
                }
                this.event.publish('adminPrivileges', data.data.adminPrivileges ? data.data.adminPrivileges : temp);
                this.storage.set('adminPrivileges', data.data.adminPrivileges ? data.data.adminPrivileges : temp);
                this.event.publish('isAdmin', isAdmin);
                this.storage.set('isAdmin', isAdmin);
                if (!this.navCtrl.canGoBack()) {
                  this.navCtrl.setRoot('HomePage');
                } else {
                  this.navCtrl.pop();
                }
                loading.dismiss();
              }
            );
          },
          (error: any) => { this.popupError(error); loading.dismiss(); }
        );
      },
      (error: any) => {
        this.errorMsg = "Salah email / password. Silahkan login kembali";
        loading.dismiss();
      }
    )
  }

  popupError(obj: any) {
    let alert = this.alrtCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
