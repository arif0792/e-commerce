import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDashboardOverduePage } from './admin-dashboard-overdue';

@NgModule({
  declarations: [
    AdminDashboardOverduePage,
  ],
  imports: [
    IonicPageModule.forChild(AdminDashboardOverduePage),
  ],
})
export class AdminDashboardOverduePageModule {}
