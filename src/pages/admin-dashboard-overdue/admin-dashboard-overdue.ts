import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular'; 0
import { AccountProvider } from '../../providers/account/account';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { Loading } from 'ionic-angular/components/loading/loading';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Order } from '../../class/Order';
import { OrderProvider } from '../../providers/order/order';

/**
 * Generated class for the AdminDashboardRefundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-dashboard-overdue',
  templateUrl: 'admin-dashboard-overdue.html',
})
export class AdminDashboardOverduePage {

  ordersArray: Array<{ order: Order, firstName: string, lastName: string }> = [];
  ordercount: number;
  loading: Loading;
  querysearch: string;
  done = false;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public accountProvider: AccountProvider, public loadingCtrl: LoadingController,
    public modalCtrl: ModalController, public alertCtrl: AlertController, public orderProvider: OrderProvider) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.querysearch = "";
    this.getOrderById(this.querysearch, 0);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminDashboardUserPage');
  }

  onEnter(value: string) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();
    this.querysearch = value;
    this.getOrderById(this.querysearch, 0);
  }

  doLoadMoreData(infiniteScroll) {
    this.getOrderById(this.querysearch, this.ordersArray.length, infiniteScroll);
  }

  getOrderById(name: string, start: number, infScroll?: any) {
    this.orderProvider.getOrdersOverDueByIDCount(name).subscribe(
      data => {
        this.ordercount = data.data;
        this.orderProvider.getOrdersOverDueByID(name, start).subscribe(
          data => {
            if (start == 0) {
              this.ordersArray = [];
            }
            for (let ord of data.data) {
              this.ordersArray.push({ order: new Order(ord.ID, ord.CreatedAt, ord.products, ord.totalInstallment, ord.totalPrice, ord.paymentDetails, ord.status, ord.orderDate, ord.isRefund, ord.userId, ord.canceledDate, ord.completedDate, ord.haveCancelDeadlinem, ord.isActive, ord.deadlineCancelMonth, ord.lebaranMonth), firstName: ord.firstName, lastName: ord.lastName });
            }
            console.log(this.ordersArray);
            this.loading.dismiss();
            infScroll ? infScroll.complete() : null;
            this.done= true;
          },
          (error: any) => {this.popupError(error); this.loading.dismiss(); }
        );
      },
      (error: any) => {this.popupError(error); this.loading.dismiss(); }
    );
  }

  sendMessage(order: any) {
    let modal = this.modalCtrl.create('ModalAdminSendMsgPage', { userId: order.order.userId, userFullName: order.firstName + " " + order.lastName }, {
      cssClass: "my-modal"
    });
    modal.present();
  }

  goToDetailUser(id: number) {
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait',
      spinner: 'dots'
    });

    this.loading.present();

    this.accountProvider.getUserByID(id).subscribe(
      data => {
        this.navCtrl.push('AdminDetailUserPage', {
          user: data.data
        });
        this.loading.dismiss();
      }
    );
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj.message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  getCurrency(num: number): string {
    return num.toLocaleString(undefined, { minimumFractionDigits: 0 }).replace(/,/g, '.');
  }

}
