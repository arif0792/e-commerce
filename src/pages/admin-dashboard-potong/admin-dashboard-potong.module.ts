import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminDashboardPotongPage } from './admin-dashboard-potong';

@NgModule({
  declarations: [
    AdminDashboardPotongPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminDashboardPotongPage),
  ],
})
export class AdminDashboardPotongPageModule {}
