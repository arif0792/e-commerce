import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CodePush, SyncStatus } from '@ionic-native/code-push';

/**
 * Generated class for the UpdatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html',
})
export class UpdatePage {
  progress: any;
  onUpdate = false;
  progressString: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public codePush: CodePush) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdatePage');
  }

  update() {
    this.onUpdate = true;
    const downloadProgress = (progress) => { 
      this.progress = Math.round((progress.totalBytes / progress.receivedBytes) * 100);
      this.progressString = "Downloaded "+progress.receivedBytes+" of "+progress.totalBytes; 
    }
    this.codePush.sync({}, downloadProgress).subscribe(
      (syncStatus) => {
        console.log(syncStatus);
        if(syncStatus == SyncStatus.UPDATE_INSTALLED){
          this.codePush.restartApplication();
        }
      });
  }

}
