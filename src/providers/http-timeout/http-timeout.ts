import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/timeout';

@Injectable()
export class AngularInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).timeout(20000).do(event => { }, err => { // timeout of 20 seconds
      if (err instanceof HttpErrorResponse) {
        console.log("Error Caught By Interceptor");
        Observable.throw("Service Timeout");
      }
    });
  }
}