import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../../class/User';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { Response } from '../../class/Response';
import { REST } from '../../enum/rest';
import { Storage } from '@ionic/storage';

/*
  Generated class for the AccountProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AccountProvider {

  constructor(public httpClient: HttpClient, public storage: Storage) {
    console.log('Hello AccountProvider Provider');
  }

  login(account: User): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_ACCOUNT_LOGIN,
      JSON.stringify(account), { headers: { 'Content-Type': 'application/json' } })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  postSaveOrUpdateAccount(account: User): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.GET_POST_ACCOUNT,
      JSON.stringify(account),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  postChangePasswordAccount(account: User): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_CHANGE_PASS_ACCOUNT,
      JSON.stringify(account),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  putUpdateFcmTokenUser(ID: string, token: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.put<Response>(JSON.parse(data) + REST.PUT_ACCOUNT_FCM_TOKEN
      , {
        userId: ID,
        userToken: token
      }
    )
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  userNameCheck(account: User): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_ACCOUNT_USERCHECK,
      JSON.stringify(account),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  emailCheck(account: User): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_ACCOUNT_EMAILCHECK,
      JSON.stringify(account),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  phoneNumberCheck(account: User): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_ACCOUNT_PHONENUMBERCHECK,
      JSON.stringify(account),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  passwordCheck(account: User): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_ACCOUNT_PASSWORDCHECK,
      JSON.stringify(account),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getUsersByNameCount(name: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ACCOUNT_BYNAME_COUNT
      , {
        params: {
          name: name
        }
      }
    )
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getUsersByName(name: string, start: number): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ACCOUNT_BYNAME
      , {
        params: {
          name: name,
          start: start.toString()
        }
      }
    )
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getUserByID(id: number): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_POST_ACCOUNT
      , {
        params: {
          id: id.toString()
        }
      }
    )
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

}
