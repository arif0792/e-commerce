import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Address } from '../../class/Address';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { Response } from '../../class/Response';
import { REST } from '../../enum/rest';
import { Storage } from '@ionic/storage';

/*
  Generated class for the AddressProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AddressProvider {

  constructor(public httpClient: HttpClient, public storage: Storage) {
    console.log('Hello AddressProvider Provider');
  }

  deleteAddress(addr: Address): Observable<Address> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.delete<Response>(JSON.parse(data) + REST.POST_DELETE_ADDRESS, {
      params: {
        addressID: addr.ID.toString()
      }
    })
      .map(data => {
        console.log(data);
        return data.data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  postAddress(addr: Address): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_DELETE_ADDRESS,
      JSON.stringify(addr),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getAddresses(ID: string): Observable<Address[]> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ADDRESSES
      , {
        params: {
          userId: ID
        }
      }
    )
      .map(data => {
        console.log(data);
        return data.data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

}
