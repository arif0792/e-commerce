import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { GeocodingResponse } from '../../class/googleAPI/GeoCoding/GeocodingResponse';
import { googleMapAPI } from '../../assets/config/apikeys';

/*
  Generated class for the GoogleApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GoogleApiProvider {

  apiUrl = "https://maps.googleapis.com/maps/api/geocode/json";

  constructor(public http: HttpClient) {
    console.log('Hello GoogleApiProvider Provider');
  }

  postGeoCodingInquiry(address: string): Observable<GeocodingResponse> {
    return this.http.get<GeocodingResponse>(this.apiUrl,
      {
        params: {
          address: address,
          key: googleMapAPI.key
        }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      )
  }
}
