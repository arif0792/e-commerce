import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Response } from '../../class/Response';
import { REST } from '../../enum/rest';
import { Storage } from '@ionic/storage';
import 'rxjs/add/observable/throw';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SettingsProvider {

  constructor(public http: HttpClient, public storage: Storage) {
    console.log('Hello SettingsProvider Provider');
  }

  getSettingLebaranMonth(): Observable<number> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.get<Response>(JSON.parse(data) + REST.GET_PARAM_LEBARAN)
      .map(data => {
        return parseInt(data.data['settingValue'])
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  setSettingLebaranMonth(value: string) {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.post<Response>(JSON.parse(data) + REST.POST_PARAM_SETTING,
      {
        settingName: "LebaranMonth",
        settingValue: value
      })
      .map((data) => {
        console.log(data);
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getSettingDeadlineCancel(): Observable<number> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.get<Response>(JSON.parse(data) + REST.GET_PARAM_DEADLINE_CANCEL)
      .map(data => {
        return parseInt(data.data['settingValue']);
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  setSettingDeadlineCancel(value: string) {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.post<Response>(JSON.parse(data) + REST.POST_PARAM_SETTING,
      {
        settingName: "DeadlineMonth",
        settingValue: value
      })
      .map((data) => {
        console.log(data);
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getSettingBannerPicIds(): Observable<string> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.get<Response>(JSON.parse(data) + REST.GET_PARAM_BANNER_PICS)
      .map(data => {
        return data.data['settingValue'];
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  setSettingBannerPicIds(value: string) {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.post<Response>(JSON.parse(data) + REST.POST_PARAM_SETTING,
      {
        settingName: "BannerPicIds",
        settingValue: value
      })
      .map((data) => {
        console.log(data);
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }
}
