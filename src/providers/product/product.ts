import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../../class/Product';
import { Response } from '../../class/Response';
import { REST } from '../../enum/rest';
import { Observable } from 'rxjs/Observable';
import { Storage } from '@ionic/storage';
import 'rxjs/add/observable/throw';

/*
  Generated class for the ProductProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ProductProvider {

  constructor(public httpClient: HttpClient, public storage: Storage) {
    console.log('Hello ProductProvider Provider');
  }

  saveProducts(prods: Product[]): Observable<Product[]> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_PRODUCTS,
      JSON.stringify(prods),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data.data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getProductOrderByKelurahanCount(name: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_PRODUCTS_BY_KELURAHAN_COUNT,
      {
        params: {
          name: name
        }
      }
    ).map(data => {
      console.log(data);
      return data;
    }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getProductOrderByKelurahan(name: string, start: number): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_PRODUCTS_BY_KELURAHAN,
      {
        params: {
          name: name,
          start: start.toString()
        }
      }
    ).map(data => {
      console.log(data);
      return data;
    }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getProductOrderByPotongCount(name: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_PRODUCTS_BY_POTONG_COUNT,
      {
        params: {
          name: name
        }
      }
    ).map(data => {
      console.log(data);
      return data;
    }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getProductOrderByPotong(name: string, start: number): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_PRODUCTS_BY_POTONG,
      {
        params: {
          name: name,
          start: start.toString()
        }
      }
    ).map(data => {
      console.log(data);
      return data;
    }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

}
