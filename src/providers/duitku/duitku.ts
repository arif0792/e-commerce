import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Observable';
import { ResponseDuitkuInquiry } from '../../class/duitku/ResponseDuitkuInquiry';
import { RequestDuitkuInquiry } from '../../class/duitku/RequestDuitkuInquiry';
import { Storage } from '@ionic/storage';
import { REST } from '../../enum/rest';
import { Response } from '../../class/Response';

/*
  Generated class for the DuitkuProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DuitkuProvider {

  constructor(public http: HttpClient, public storage: Storage) {
    console.log('Hello DuitkuProvider Provider');
  }

  postInquiry(obj: RequestDuitkuInquiry): Observable<ResponseDuitkuInquiry> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.get<Response>(JSON.parse(data) + REST.INIT_PAYMENT_GATEWAY,
      {
        params: {
          paymentAmount: obj.paymentAmount.toString(),
          paymentMethod: obj.paymentMethod,
          merchantOrderId: obj.merchantOrderId,
          productDetail: obj.productDetail,
          signature: obj.signature,
          email: obj.email
        }
      })
      .map(data => data.data)
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }
}
