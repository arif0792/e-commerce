import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Platform } from 'ionic-angular';
import 'rxjs/add/observable/throw';

/*
  Generated class for the CountryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CountryProvider {
 
  private apiUrl: string;

  constructor(public http: HttpClient, public platform: Platform ) {
    console.log('Hello CountryProvider Provider');
    if( this.platform.is('android') || this.platform.is('ios') ) {
      this.apiUrl = './assets/js/countries.json';
    } else {
      this.apiUrl = '../assets/js/countries.json';
    }
  }

  getCountries(): Observable<Array<{name: string, code: string}>> {
    return this.http.get<Array<{name: string, code: string}>>(this.apiUrl)
      .map(data => data)
      .catch(this.handleError);
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
