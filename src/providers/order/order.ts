import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Order } from '../../class/Order';
import { REST } from '../../enum/rest';
import { Response } from '../../class/Response';
import { Storage } from '@ionic/storage';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';

/*
  Generated class for the OrderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OrderProvider {

  constructor(public httpClient: HttpClient, public storage: Storage) {
    console.log('Hello OrderProvider Provider');
  }

  postCancelOrder(order: Order): Observable<Response> {
    order.canceledDate = new Date();
    order.status = 3;
    if (order.paymentDetails && order.paymentDetails.length > 0) {
      order.isRefund = true;
    }
    return this.postOrder(order);
  }

  postOrder(order: Order): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_DELETE_ORDER,
      JSON.stringify(order),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(
      data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  deleteOrder(order: Order): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.delete<Response>(JSON.parse(data) + REST.POST_DELETE_ORDER,
      {
        params: {
          orderId: order.ID.toString()
        }
      })
      .map(
      data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getOrders(ID: string): Observable<Order[]> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ORDERS
      , {
        params: {
          userId: ID
        }
      }
    ).map(
      data => {
        console.log(data);
        return data.data;
      }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getOrdersRefundByIDCount(ID: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ORDERS_REFUND_BY_ID_COUNT
      , {
        params: {
          id: ID
        }
      }
    ).map(
      data => {
        console.log(data);
        return data;
      }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getOrdersRefundByID(ID: string, start: number): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ORDERS_REFUND_BY_ID
      , {
        params: {
          id: ID,
          start: start.toString()
        }
      }
    ).map(
      data => {
        console.log(data);
        return data;
      }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getOrdersOverDueByIDCount(ID: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ORDERS_OVERDUE_BY_ID_COUNT
      , {
        params: {
          id: ID
        }
      }
    ).map(
      data => {
        console.log(data);
        return data;
      }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getOrdersOverDueByID(ID: string, start: number): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ORDERS_OVERDUE_BY_ID
      , {
        params: {
          id: ID,
          start: start.toString()
        }
      }
    ).map(
      data => {
        console.log(data);
        return data;
      }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getOrdersHistoryByIDCount(ID: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ORDERS_HISTORY_BY_ID_COUNT
      , {
        params: {
          id: ID
        }
      }
    ).map(
      data => {
        console.log(data);
        return data;
      }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getOrdersHistoryByID(ID: string, start: number): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ORDERS_HISTORY_BY_ID
      , {
        params: {
          id: ID,
          start: start.toString()
        }
      }
    ).map(
      data => {
        console.log(data);
        return data;
      }).catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }


}
