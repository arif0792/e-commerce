import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { Platform } from 'ionic-angular/platform/platform';

/*
  Generated class for the ConfigProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConfigProvider {
  url: string;
  urlConfig: string;
  private _config;
  private _env;

  constructor(public http: HttpClient, public platform: Platform) {
    console.log('Hello ConfigProvider Provider');
    if (this.platform.is('ios') || this.platform.is('android')) {
      this.url = "./assets/config/env.json";
      this.urlConfig = "./assets/config/";
    } else {
      this.url = "../assets/config/env.json";
      this.urlConfig = "../assets/config/";
    }
  }

  load() {
    return new Promise((resolve, reject) => {
      this.http.get(this.url)
        .subscribe(env_data => {
          this._env = env_data;
          this.http.get(this.urlConfig + this._env.env + '.json')
            .catch((error: any) => {
              console.error(error);
              return Observable.throw(error || 'Server error');
            })
            .subscribe(data => {
              console.log(data);
              this._config = data;
              resolve(true);
            },
            (error: any) => console.log(error)
            );
        },
        (error: any) => console.log(error)
        );
    });
  }

  getEnv(key: any) {
    return this._env[key];
  }

  get(key: any) {
    console.log(this._config);
    return this._config[key];
  }

}
