import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { REST } from '../../enum/rest';
import { Storage } from '@ionic/storage';
import { Response } from '../../class/Response';
import { ItemDetail } from '../../class/ItemDetail';

/*
  Generated class for the CattleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ItemProvider {

  private apiUrl: string;

  constructor(public http: HttpClient, public storage: Storage) {
    console.log('Hello CattleProvider Provider');
  }

  getItemsByCategory(id: string): Observable<ItemDetail[]> {
    console.log(this.apiUrl);
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.get<Response>(JSON.parse(data) + REST.GET_PARAM_ITEMS_BY_CATEGORY,
      {
        params: {
          id: id
        }
      })
      .map(data => data.data)
      .catch(
        (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getAllItems(): Observable<ItemDetail[]> {
    console.log(this.apiUrl);
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.get<Response>(JSON.parse(data) + REST.GET_POST_PARAM_ALL_ITEMS)
      .map(data => data.data)
      .catch(
        (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  deleteItem(id: string): Observable<ItemDetail> {
    console.log(this.apiUrl);
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.delete<Response>(JSON.parse(data) + REST.DELETE_ITEM,
      {
        params: {
          id: id
        }
      })
      .map(data => data.data)
      .catch(
        (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  saveItems(itemDetails: ItemDetail[]): Observable<Response> {
    console.log(JSON.stringify(itemDetails));
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.post<Response>(JSON.parse(data) + REST.GET_POST_PARAM_ALL_ITEMS,
      JSON.stringify(itemDetails),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(
        data => {
          console.log(data);
          return data;
        })
      .catch(
        (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getItemPictures(ids: string[]): Observable<Response> {
    let httpParams = new HttpParams();
    ids.forEach(id => {
      httpParams = httpParams.append('id', id);
    });
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.get<Response>(JSON.parse(data) + REST.GET_POST_ITEM_PICTURES,
      {
        params: httpParams
      }
    )
      .map(data => data)
      .catch(
        (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  postItemPictures(pics: Array<{ ID: number, data: string }>): Observable<Response> {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.post<Response>(JSON.parse(data) + REST.GET_POST_ITEM_PICTURES,
      JSON.stringify(pics), {headers: headers}
    )
      .map(data => data)
      .catch(
        (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  deleteItemPicture(id: string): Observable<Response> {
    console.log(this.apiUrl);
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.http.delete<Response>(JSON.parse(data) + REST.DELETE_ITEM_PICTURE,
      {
        params: {
          picID: id
        }
      })
      .map(data => data.data)
      .catch(
        (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

}
