import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { Storage } from '@ionic/storage';
import { REST } from '../../enum/rest';
import { Response } from '../../class/Response';

/*
  Generated class for the MessageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MessageProvider {

  constructor(public httpClient: HttpClient, public storage: Storage) {
    console.log('Hello MessageProvider Provider');
  }

  postMessage(msg: { from: string, data: string, userId: number }): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_ACCOUNT_MESSAGE,
      JSON.stringify(msg),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getMessages(ID: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ACCOUNT_MESSAGES
      , {
        params: {
          userId: ID
        }
      }
    )
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  postRead(msg: { read: boolean, messageId: number, userId: number }): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_ACCOUNT_READ,
      JSON.stringify(msg),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getReads(ID: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_ACCOUNT_READS
      , {
        params: {
          userId: ID
        }
      }
    )
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  postInquiry(from: string, msg: string): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.post<Response>(JSON.parse(data) + REST.POST_INQUIRY,
      JSON.stringify({ from: from, data: msg }),
      {
        headers: { 'Content-Type': 'application/json' }
      })
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

  getInquiries(): Observable<Response> {
    return Observable.fromPromise(this.storage.get('apiUrl').then(data => this.httpClient.get<Response>(JSON.parse(data) + REST.GET_INQUIRIES
    )
      .map(data => {
        console.log(data);
        return data;
      })
      .catch(
      (error: any) => Observable.throw(error || 'Server error')
      ))).mergeMap(data => data);
  }

}
