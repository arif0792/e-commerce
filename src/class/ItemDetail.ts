export class ItemDetail{
    constructor(public ID: number, public itemPrice: number, public itemAdminPrice: number, public itemDeliveryPrice: number, public itemCategory: string,
        public itemName: string, public haveCancelDeadline: boolean, public itemPictureIds: string){}
}