export class Product{
    constructor(public ID: number, public CreatedAt: Date, public productId: string, public status: number, public alamatId: number, public orderId: number,
        public itemName: string, public itemPrice: number, public itemAdminPrice: number, public itemOptionPrice: number, public itemCategory: string, public itemOption: string){}
}