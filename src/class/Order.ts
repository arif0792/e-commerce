import { Product } from "./Product";
import { Status } from "../enum/status";
import { PaymentDetail } from "./PaymentDetail";

export class Order {

    constructor(public ID: number, public CreatedAt: Date, public products: Product[], public totalInstallment: number, public totalPrice: number, public paymentDetails: PaymentDetail[], public status: number, 
        public orderDate: Date, public isRefund: boolean, public userId: number, public canceledDate: Date, public completedDate: Date, public haveCancelDeadline: boolean, public isActive: boolean, 
        public deadlineCancelMonth: number, public lebaranMonth?: number) {

    }

    getProdDesc(): string[] {
        let map: Map<string, number> = new Map<string, number>();
        for (let obj of this.products) {
            map.set(obj.itemName, map.get(obj.itemName) ? map.get(obj.itemName) + 1 : 1);
        }
        let results: string[] = [];
        map.forEach((value: number, key: string) => {
            results.push(value + " x " + key);
        });
        return results;
    }

    getSisaInstallment(): number {
        return this.totalInstallment - this.getCurrentInstallment();
    }

    getCicilan(): number {
        return (this.totalPrice / this.totalInstallment);
    }

    getStatusName(): string {
        if (Status[this.status] == 'WAITING_FOR_PAYMENT') {
            return "WAITING FOR PAYMENT";
        } else if (Status[this.status] == 'ON_DELIVERY') {
            return "ON DELIVERY"
        } else {
            return Status[this.status];
        }
    }

    getRefund(): number {
        let dateDeadline = new Date();
        let dateCanceledDate = new Date(this.canceledDate);

        if (this.haveCancelDeadline) {
            if (this.lebaranMonth && this.lebaranMonth > 0) {
                dateDeadline.setMonth(this.lebaranMonth - this.deadlineCancelMonth);
            } else {
                let orderDateDATE = new Date(this.orderDate);
                dateDeadline = new Date(orderDateDATE.getFullYear(), orderDateDATE.getMonth(), orderDateDATE.getDate());
                dateDeadline.setMonth(dateDeadline.getMonth() + this.totalInstallment - this.deadlineCancelMonth);
            }

            if (this.status === Status.CANCELED
                && this.getCurrentInstallment() !== 0) {

                if (dateDeadline < dateCanceledDate) {
                    let sumAdminFee = 0;
                    for (let prd of this.products) {
                        sumAdminFee += prd.itemAdminPrice;
                    }
                    return this.getPayedPayment() - sumAdminFee;
                } else {
                    return this.getPayedPayment();
                }

            }
        } else {
            if (this.status === Status.CANCELED
                && this.getCurrentInstallment() !== 0) {
                return this.getPayedPayment();
            }
        }
        return null;
    }

    getPayedPayment(): number{
        let sum = 0;
        for(let pay of this.paymentDetails){
            sum += pay.paymentDuitkuDetail.paymentAmount
        }
        return sum;
    }

    getCurrentInstallment(): number {
        return this.paymentDetails ? (this.paymentDetails.length > this.totalInstallment ? this.totalInstallment : this.paymentDetails.length) : 0;
    }

    getOverdue() {
        let dateNow = new Date();
        let firstDayDateNow = new Date(dateNow.getFullYear(), dateNow.getMonth(), 1);
        //
        let orderDateDATE = new Date(this.orderDate);
        let overDueDate = new Date(orderDateDATE.getFullYear(), orderDateDATE.getMonth() + this.getCurrentInstallment(), orderDateDATE.getDate());

        let Nomonths = (firstDayDateNow.getFullYear() - overDueDate.getFullYear()) * 12;
        Nomonths -= overDueDate.getMonth() + 1;
        Nomonths += firstDayDateNow.getMonth() + 1; // we should add + 1 to get correct month number

        return Nomonths <= 0 ? 0 : Nomonths;;
    }

    getTunggakan(): number {
        return this.getCicilan() * (this.getOverdue() + 1 <= this.getCurrentInstallment() ? this.getOverdue() + 1 : this.getCurrentInstallment());
    }

    getTunggakanInstallment(): number {
        return this.getOverdue() + 1 <= this.getCurrentInstallment() ? this.getOverdue() + 1 : this.getCurrentInstallment();
    }
}