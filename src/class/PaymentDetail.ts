import { PaymentDuitkuDetail } from "./duitku/PaymentDuitkuDetail";

export class PaymentDetail {
    constructor(public ID: number, public payMethod: string, public installmentNumber: number, public orderId: number,
        public paymentDate: Date, public paymentDuitkuDetail?: PaymentDuitkuDetail){}
}