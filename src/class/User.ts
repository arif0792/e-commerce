import { Order } from "./Order";
import { Address } from "./Address";

export class User{
    constructor(public ID: number, public firstName: string, public lastName: string, public email: string,
        public phoneNumber: string, public userName: string, public password: string, public orders: Order[], public addresses: Address[],
        public messages: Array<{ID: number, from: string, data: string, userId?: number}>,
        public reads: Array<{ID: number,read: boolean, messageId: number, userId: number}>,
        public adminPrivileges?: {dashboardOrder: boolean,	dashboardDelivery: boolean, dashboardRefund: boolean, dashboardOverdue: boolean,
            dashboardHistory: false, broadcastMessage: false, inquiry: false, settings: boolean}){}
}