export class Address{
    constructor(public ID: number, public addressAlias: string,
         public address: string, public postalCode: number, public homeNo: string, public kelurahan: string, public userId: number){}
}