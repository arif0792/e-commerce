export class Payment_CreditCard{
    constructor(public ID: number,public firstName: string,public lastName: string,public billingAddress: string,public country: string, 
        public city: string, public zipcode: number, public phoneNumber: number,public cardType: string,public cardNumber: string, 
        public cardExpDate: string,public cardSecurityCode: number,public paymentDate: Date,public paymentDetailId: number){}
}