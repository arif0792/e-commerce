export class GeocodingResponse {
    constructor(
        public results: Array<{
            address_components: Array<{
                long_name: string,
                short_name: string,
                types: Array<string>
            }>,
            formatted_address: string,
            geometry: {
                location: {
                    lat: number, lng: number
                },
                location_type: string,
                viewport: {
                    northeast: {
                        lat: number, lng: number
                    },
                    southwest: {
                        lat: number, lng: number
                    }
                }
            },
            place_id: string,
            types: Array<string>
        }>,
        public status: string
    ) { }
}