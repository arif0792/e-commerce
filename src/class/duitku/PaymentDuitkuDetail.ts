export class PaymentDuitkuDetail {

    constructor(public ID: number, public refID: string, public resultCode: string, public paymentMethod: string,
        public paymentAmount: number, public installmentNumber: number, public	orderID: number, public paymentDetailId: number, public vaNumber?: string){
    }
}