export class ResponseDuitkuInquiry{
    constructor(public merchantCode: string,public reference: string,public vaNumber: string, public paymentUrl: string, public statusCode: string, public statusMessage: string){}
}