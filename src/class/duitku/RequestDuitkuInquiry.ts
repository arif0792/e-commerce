export class RequestDuitkuInquiry {

    constructor(public paymentAmount: number, public merchantOrderId: string, public productDetail: string,
        public email: string, public paymentMethod: string,
        public signature: string) {
    }
}