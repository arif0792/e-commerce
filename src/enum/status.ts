export enum Status {
    WAITING_FOR_PAYMENT,
    ACTIVE,
    COMPLETED,
    CANCELED
}