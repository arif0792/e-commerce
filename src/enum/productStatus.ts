export enum PRODUCT_STATUS {
    ON_PROGRESS,
    ON_DELIVERY,
    COMPLETED
}