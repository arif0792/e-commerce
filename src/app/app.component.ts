import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Badge } from '@ionic-native/badge';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular/util/events';
import { ConfigProvider } from '../providers/config/config';
import { AccountProvider } from '../providers/account/account';
import { SettingsProvider } from '../providers/settings/settings';
import { AngularFireAuth } from 'angularfire2/auth';
import { FCM } from '@ionic-native/fcm';
import { User } from '../class/User';
import { PaymentDuitkuDetail } from '../class/duitku/PaymentDuitkuDetail';
import { Order } from '../class/Order';
import { Product } from '../class/Product';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { CodePush } from '@ionic-native/code-push';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  driverName = "";
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'HomePage';

  userData: User;
  hasLoggedIn: boolean;
  messagesBadge: number;
  adminPrivileges: {
    dashboardPotong: boolean, dashboardDelivery: boolean, dashboardUser: boolean, dashboardOverdue: boolean,
    dashboardRefund: boolean, dashboardHistory: boolean, broadcastMessage: boolean, inquiry: boolean, settings: boolean
  } = null;
  isAdmin: boolean = false;

  NavPages: Array<{ title: string, name: string, component: any, icon: string }>;
  NavUserPages: Array<{ title: string, name?: string, component?: any, icon?: string }>;
  NavGuestPages: Array<{ title: string, name?: string, component?: any, icon?: string }>;
  NavAdminPages: Array<{ title: string, name?: string, component?: any, icon?: string, hidden: boolean }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
    public alertCtrl: AlertController, public storage: Storage, public event: Events, public menu: MenuController,
    public configProvider: ConfigProvider, public badge: Badge, public accountProvider: AccountProvider,
    public settingsProvider: SettingsProvider, public firebase: AngularFireAuth, public fcm: FCM, public codePush: CodePush) {

    this.storage.ready().then(
      () =>
        this.driverName = this.storage.driver
    );
    //default
    this.adminPrivileges = { dashboardPotong: false, dashboardDelivery: false, dashboardUser: false, dashboardOverdue: false, dashboardRefund: false, dashboardHistory: false, broadcastMessage: false, inquiry: false, settings: false };

    this.event.subscribe('login', (data) => {
      this.hasLoggedIn = data;
      this.storage.get('userData').then(
        data => {
          this.userData = JSON.parse(data);
          this.fcm.subscribeToTopic('userId' + this.userData.ID.toString());
          this.storage.set('hasLoggedIn', JSON.stringify(true));
          this.registerTokenFcm(this.userData.ID.toString()).then(
            () => {
              console.log("Sukses register token");
            }
          ).catch(
            (error: any) => { console.log("Error registerTokenFcm"); }
          );
        }
      )
    });
    this.event.subscribe('messagesBadge', (data) => {
      this.messagesBadge = data;
      this.badge.set(data);
    });
    this.event.subscribe('adminPrivileges', (data) => {
      if (data != null) {
        this.setAdminPrivs(data);
      }
    });
    this.event.subscribe('isAdmin', (data) => {
      if (data != null) {
        this.isAdmin = data;
      }
    });
    this.storage.get('messagesBadge').then(
      data => this.messagesBadge = data
    );
    this.storage.get('hasLoggedIn').then(
      data => {
        this.firebase.authState.subscribe(
          user => {
            if (JSON.parse(data) == true && user) {
              this.firebase.auth.currentUser.getIdToken(true).then(
                () => {
                  this.storage.get('userData').then(
                    userData => {
                      if (userData) {
                        this.userData = JSON.parse(userData);
                        this.hasLoggedIn = JSON.parse(data);
                        this.registerTokenFcm(this.userData.ID.toString()).then(
                          () => {
                            console.log("Sukses register token");
                          }
                        ).catch(
                          (error: any) => { console.log("Error registerTokenFcm"); }
                        );;
                      }
                    }
                  );
                },
                (error: any) => { this.popupError(error); }
              );
            }
          },
          (error: any) => { this.popupError(error); }
        );
      }
    );
    this.storage.get('adminPrivileges').then(
      data => {
        if (data != null) {
          this.setAdminPrivs(data);
        }
      }
    );
    this.storage.get('isAdmin').then(
      data => {
        if (data != null) {
          this.isAdmin = data;
        }
      }
    );
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.NavPages = [
      { title: 'Beranda', name: 'HomePage', component: 'HomePage', icon: 'home' },
      { title: 'FAQ', name: 'FaqPage', component: 'FaqPage', icon: 'help' },
      { title: 'Hubungi Kami', name: 'ContactUsPage', component: 'ContactUsPage', icon: 'send' }
    ];

    this.NavUserPages = [
      { title: 'Dashboard', name: 'DashboardPage', component: 'DashboardPage', icon: 'analytics' },
      { title: 'Setting Akun', name: 'AccountSettingPage', component: 'AccountSettingPage', icon: 'home' },
      { title: 'Pesan', name: 'MessagesPage', component: 'MessagesPage', icon: 'mail' },
    ];

    this.NavGuestPages = [
      { title: 'Masuk', name: 'LoginPage', component: 'LoginPage', icon: 'key' },
      { title: 'Daftar', name: 'SignupPage', component: 'SignupPage', icon: 'create' }
    ];

    this.NavAdminPages = [
      { title: 'User', name: 'AdminDashboardUserPage', component: 'AdminDashboardUserPage', icon: 'clipboard', hidden: true },
      { title: 'Potong', name: 'AdminDashboardPotongPage', component: 'AdminDashboardPotongPage', icon: 'clipboard', hidden: true },
      { title: 'Delivery', name: 'AdminDashboardDeliveryPage', component: 'AdminDashboardDeliveryPage', icon: 'clipboard', hidden: true },
      { title: 'Refund', name: 'AdminDashboardRefundPage', component: 'AdminDashboardRefundPage', icon: 'clipboard', hidden: true },
      { title: 'Overdue', name: 'AdminDashboardOverduePage', component: 'AdminDashboardOverduePage', icon: 'clipboard', hidden: true },
      { title: 'History', name: 'AdminDashboardHistoryPage', component: 'AdminDashboardHistoryPage', icon: 'clipboard', hidden: true },
      { title: 'Broadcast', name: 'AdminBroadcastPage', component: 'AdminBroadcastPage', icon: 'clipboard', hidden: true },
      { title: 'Inquiry', name: 'AdminInquiryPage', component: 'AdminInquiryPage', icon: 'clipboard', hidden: true },
      { title: 'Settings', name: 'AdminSettingsPage', component: 'AdminSettingsPage', icon: 'clipboard', hidden: true }
    ];


  }

  setAdminPrivs(data: any) {
    this.adminPrivileges = data;
    for (let page of this.NavAdminPages) {
      if (page.title == 'User') {
        page.hidden = !this.adminPrivileges.dashboardUser;
      } else if (page.title == 'Potong') {
        page.hidden = !this.adminPrivileges.dashboardPotong;
      } else if (page.title == 'Delivery') {
        page.hidden = !this.adminPrivileges.dashboardDelivery;
      } else if (page.title == 'Refund') {
        page.hidden = !this.adminPrivileges.dashboardRefund;
      } else if (page.title == 'Overdue') {
        page.hidden = !this.adminPrivileges.dashboardOverdue;
      } else if (page.title == 'History') {
        page.hidden = !this.adminPrivileges.dashboardHistory;
      } else if (page.title == 'Broadcast') {
        page.hidden = !this.adminPrivileges.broadcastMessage;
      } else if (page.title == 'Inquiry') {
        page.hidden = !this.adminPrivileges.inquiry;
      } else if (page.title == 'Settings') {
        page.hidden = !this.adminPrivileges.settings;
      }
    }
  }

  initializeApp() {
    // //add load lebaranMonth to initapp reuseable
    // this.settingsProvider.loadSettingLebaranMonth().subscribe(
    //   () => {
    //     this.storage.set('settingLebaranMonth', this.settingsProvider.getSettingLebaranMonth());
    //   }
    // );

    // //add load deadline cancel to initapp reuseable
    // this.settingsProvider.loadSettingDeadlineCancel().subscribe(
    //   () => {
    //     this.storage.set('settingDeadlineCancel', this.settingsProvider.getSettingDeadlineCancel());
    //   }
    // );

    this.configProvider.load().then(data => {
      let url: string = this.configProvider.get('apiUrl');
      this.storage.set('apiUrl', JSON.stringify(url)).then(
        () => {
          this.codePush.checkForUpdate().then(
            update => {
              if (update) {
                this.platform.ready().then(() => {
                  this.nav.setRoot('UpdatePage').then(
                    () => {
                      this.statusBar.styleLightContent();
                      this.splashScreen.hide();
                    }
                  );
                });
              } else {
                this.platform.ready().then(() => {
                  this.nav.setRoot(this.rootPage);
                  // Okay, so the platform is ready and our plugins are available.
                  // Here you can do any higher level native things you might need.
                  //FCM
                  this.fcm.subscribeToTopic('broadcast');
                  this.fcm.onNotification().subscribe(data => {
                    this.storage.get('userData').then(
                      userData => {
                        this.userData = JSON.parse(userData);
                        if (data.message) {
                          this.setMessage(data);
                        }
                        if (data.duitkuDetail) {
                          this.setDuitkuDetail(data);
                        }
                        if (data.order) {
                          this.setOrder(data);
                        }
                        if (data.product) {
                          this.setProduct(data);
                        }
                        if (data.wasTapped) {
                          if (this.hasLoggedIn) {
                            this.nav.setRoot('MessagesPage');
                          } else {
                            this.nav.setRoot('HomePage');
                          }
                          if (this.menu.isOpen) {
                            this.menu.toggle();
                          }
                          console.info("Received in background");
                        } else {
                          console.info("Received in foreground");
                        }
                      }
                    );
                  });
                  //
                  this.statusBar.styleLightContent();
                  this.splashScreen.hide();
                });
              }
            }
          );
        }
      );
    });

    this.platform.resume.subscribe(() => {
      console.log('[INFO] App resumed');
      if (this.hasLoggedIn) {
        this.firebase.auth.currentUser.getIdToken(true).then(
          () => {
            this.storage.get('userData').then(
              userData => {
                this.userData = JSON.parse(userData);
                this.registerTokenFcm(this.userData.ID.toString()).then(
                  () => {
                    console.log("Sukses register token");
                  }
                ).catch(
                  (error: any) => { console.log("Error registerTokenFcm"); }
                );;
                this.hasLoggedIn = true;
              }
            );
          },
          (error: any) => {
            this.popupError(error);
            this.hasLoggedIn = false;
          }
        );
      }
    });
  }

  registerTokenFcm(id: string): Promise<{}> {
    return new Promise((resolve, reject) => {
      //FCM
      this.fcm.getToken().then(token => {
        this.accountProvider.putUpdateFcmTokenUser(id, token).subscribe(
          () => resolve(),
          (error: any) => { this.popupError(error); reject(); }
        );
      });
      this.fcm.onTokenRefresh().subscribe(
        token => {
          this.accountProvider.putUpdateFcmTokenUser(id, token).subscribe(
            () => resolve(),
            (error: any) => { this.popupError(error); }
          );
        },
        (error: any) => { this.popupError(error); reject(); }
      );
      //
    });
  }

  setMessage(data: any) {
    this.userData.messages.splice(0, 0, JSON.parse(data.message));
    let badgeCount = 0, readCount = 0;
    for (let msg of this.userData.messages) {
      let obj: { message: { ID: number, from: string, data: string, userId?: number }, read: boolean } = { message: null, read: false };
      obj.message = msg;
      for (let msgRead of this.userData.reads) {
        if (msgRead.messageId == msg.ID
          && msgRead.read) {
          obj.read = true;
          readCount += 1;
          break;
        }
      }
    }
    badgeCount = this.userData.messages.length - readCount;
    this.event.publish('messagesBadge', badgeCount);
    this.storage.set('messagesBadge', badgeCount);
    this.storage.set('userData', JSON.stringify(<User>this.userData));
  }

  setDuitkuDetail(data: any) {
    let paymentduitkudetail: PaymentDuitkuDetail;
    paymentduitkudetail = JSON.parse(data.duitkuDetail);
    for (let o of this.userData.orders) {
      if (o.ID == paymentduitkudetail.orderID) {
        for (let p of o.paymentDetails) {
          if (p.orderId == paymentduitkudetail.orderID) {
            p.paymentDuitkuDetail = paymentduitkudetail;
            this.storage.set('userData', JSON.stringify(<User>this.userData));
            break;
          }
        }
      }
    }
  }

  setOrder(data: any) {
    let order: Order;
    order = JSON.parse(data.order);
    for (let o of this.userData.orders) {
      if (o.ID == order.ID) {
        let tempOrder = Object.assign({}, o);
        o = Object.assign({}, order);
        o.products = tempOrder.products;
        o.paymentDetails = tempOrder.paymentDetails;
        this.storage.set('userData', JSON.stringify(<User>this.userData));
        break;
      }
    }
  }

  setProduct(data: any) {
    let product: Product;
    product = JSON.parse(data.product);
    for (let o of this.userData.orders) {
      for (let p of o.products) {
        if (p.ID == product.ID) {
          p = product;
          this.storage.set('userData', JSON.stringify(<User>this.userData));
          break;
        }
      }
    }
  }

  // isActive(page: any) {
  //   if (this.nav.getActive() && this.nav.getActive().name === page.name) {
  //     console.log("test");
  //     return 'blue';
  //   }
  //   return;
  // }

  openPage(page: { title: string, name?: string, component?: any, icon?: string }) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.name);
  }

  popupError(obj: any) {
    let alert = this.alertCtrl.create({
      title: 'Server Error',
      message: obj && obj.message ? obj.message : "Error Occured",
      buttons: ['Dismiss']
    });
    alert.present();
  }

  logout() {
    let alert = this.alertCtrl.create({
      title: 'Keluar akun?',
      message: 'Apakah anda yakin ingin keluar dari akun anda?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          handler: () => {
            console.log('Tidak clicked');
          }
        },
        {
          text: 'Yakin',
          handler: () => {
            console.log('Yakin clicked');
            this.firebase.auth.signOut().then(
              () => {
                this.hasLoggedIn = false;
                this.fcm.unsubscribeFromTopic('userId' + this.userData.ID.toString());
                this.storage.remove('userData');
                this.storage.remove('adminPrivileges');
                this.event.publish('adminPrivileges', null);
                this.storage.remove('isAdmin');
                this.event.publish('isAdmin', false);
                this.storage.set('hasLoggedIn', JSON.stringify(false));
                this.nav.setRoot('HomePage');
              },
              (error: any) => { this.popupError(error); }
            );
          }
        }
      ]
    });
    alert.present();
  }
}
