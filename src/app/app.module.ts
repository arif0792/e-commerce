import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ItemProvider } from '../providers/item/item';
import { SettingsProvider } from '../providers/settings/settings';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { OrderProvider } from '../providers/order/order';
import { CountryProvider } from '../providers/country/country';
import { IonicStorageModule } from '@ionic/storage';
import { ConfigProvider } from '../providers/config/config';
import { ProductProvider } from '../providers/product/product';
import { AddressProvider } from '../providers/address/address';
import { AccountProvider } from '../providers/account/account';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DuitkuProvider } from '../providers/duitku/duitku';
import { MessageProvider } from '../providers/message/message';
import { GoogleApiProvider } from '../providers/google-api/google-api';
import { Badge } from '@ionic-native/badge';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth'
import { firebase } from '../assets/config/apikeys';
import { FCM } from '@ionic-native/fcm';
import { AngularInterceptor } from '../providers/http-timeout/http-timeout';
import { Camera } from '@ionic-native/camera';
import { CodePush } from '@ionic-native/code-push';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot({
      name: '__localdata',
         driverOrder: ['sqlite','websql']
    }),
    AngularFireModule.initializeApp(firebase),
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ItemProvider,
    SettingsProvider,
    OrderProvider,
    CountryProvider,
    ConfigProvider,
    ProductProvider,
    AddressProvider,
    AccountProvider,
    DuitkuProvider,
    MessageProvider,
    GoogleApiProvider,
    Badge,
    InAppBrowser,
    FCM,
    Camera,
    CodePush,
    [ { provide: HTTP_INTERCEPTORS, useClass: 
      AngularInterceptor, multi: true } ]
  ]
})
export class AppModule {}
